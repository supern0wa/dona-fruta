import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import settings from './settings'
import menu from './menu'
import user from './user'
import order from './order'
import wallet from './wallet'
import logger from './logger'
import revision from './revision'

export default new Vuex.Store({
	modules: {
		auth,
		settings,
		menu,
		user,
		wallet,
		order,
		logger,
		revision
	}
})
