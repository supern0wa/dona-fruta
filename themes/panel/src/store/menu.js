import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		menu: {},
		products: [],
		featured: [],
		categories: []
	},
	getters: {
		getMenu(state) {
			return state.menu
		},
		getProducts(state) {
			return state.products
		},
		getFeatured(state) {
			return state.featured
		},
		getCategories(state) {
			return state.categories
		}
	},
	actions: {
		fetchMenu({ state }, payload) {
			return new Promise((resolve, reject) => {
				// Check if we already have the products data
				if (state.products.length > 0)
					return resolve(state.menu)

				// Retrieve all products data
				Vue.http.get(`${process.env.API_URL}/v1/menu`)
					.then(response => {
						if (response.status == 200) {
							let items = response.body

							// Reset categories
							state.categories = []

							// Loop through all menu products
							items.forEach(item => {
								let product = item.product,
									category = product.category,
									object = {
										id: item.id,
										name: product.name,
										price: item.diff_value,
										suspended: product.suspended == 1 ? true : false,
										featured: product.featured == 1 ? true : false,
										show: item.show == 1 ? true : false,
										description: product.description,
										category: category.name,
										calories: product.calories,
										thumbnail: product.thumbnail
									}

								// Populate categories
								if (state.categories.indexOf(category.name) == -1)
									state.categories.push(category.name)

								// Check if menu key exists
								if (!state.menu[category.name])
									state.menu[category.name] = []

								// Push to menu & products array
								state.menu[category.name].push(object)
								state.products.push(object)

								// If featured, push to featured array
								if (object.featured)
									state.featured.push(object)

								// Return menu
								return resolve(state.menu)
							})
						} else {
							return resolve(response)
						}
					}, error => console.error(error))
			})
		},
		updateStatus (context, payload) {
			return new Promise((resolve, reject) => {
				let products = {}

				// Organize product data to backend
				payload.products.forEach(
					product => products[product.id] = product.show
				)

				Vue.http.post(`${process.env.API_URL}/v1/menu/change-status`, {
					menu_products: products
				})
					.then(response => {
						if (response.status == 200) {
							store.commit('logger/info', {
								message: 'Cardápio atualizado com sucesso.'
							})

							return resolve(response)
						}

						return reject(response)
					}, error => console.error(error))
			})
		}
	},
	mutations: {
		updateProducts (state, products) {
			state.products = products
		}
	}
}
