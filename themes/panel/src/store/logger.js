import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		last: {
			type: '',
			message: '',
			duration: 5000,
			action: 'Fechar',
			callback: () => {}
		},
		history: [],
		show: false
	},
	getters: {
		getLast (state) {
			return state.last
		},
		getShow (state) {
			return state.show
		}
	},
	mutations: {
		info (state, payload) {
			store.commit('logger/send', {
				type: 'info',
				message: payload.message,
				duration: payload.duration ? payload.duration : 5000,
				action: payload.action ? payload.action : 'Fechar',
				callback: payload.callback ? payload.callback : () => {}
			})
		},
		error (state, payload) {
			store.commit('logger/send', {
				type: 'error',
				message: payload.message,
				duration: payload.duration ? payload.duration : 5000,
				action: payload.action ? payload.action : 'Fechar',
				callback: payload.callback ? payload.callback : () => {}
			})
		},
		send (state, message) {
			state.history.push(message)
			state.last = message
			state.show = true
		},
		setShow (state, value) {
			state.show = value
		}
	}
}
