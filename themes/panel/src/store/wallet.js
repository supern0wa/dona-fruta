import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		month: 0,
		wallet: {
			credit: 0,
			available: 0,
			spent: 0,
			weeks: {
				1: 0,
				2: 0,
				3: 0,
				4: 0,
				5: 0
			}
		}
	},
	getters: {
		getWallet (state) {
			return state.wallet
		}
	},
	actions: {
		fetchWallet ({ state }, month) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/user/wallet/${month ? month : ''}`)
					.then(response => {
						if (response.status == 200) {
							state.month = month
							state.wallet = response.body.wallet

							return resolve(state.wallet)
						}

						return reject(response)
					}, error => console.error(error))
			})
		},
	}
}
