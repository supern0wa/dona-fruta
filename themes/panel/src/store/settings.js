import Vue from 'vue'

export default {
	namespaced: true,
	state: {
		scheduler: {
			lockOnEmpty: true,
			showArrows: false,
			max: 50,
			advance: 1,
			days: [1, 2, 3, 4, 5]
		}
	},
	actions: {
		fetchDate ({ state }, payload) {
			return new Promise((resolve, reject) => {
				let moment = require('moment')

				Vue.http.get(`${process.env.API_URL}/v1/date`)
				.then(response => {
					let date = response.body.date

					// Create now method
					moment.today = function() {
						return moment(date)
					}

					return resolve(moment.today)
				})
			})
		}
	}
}
