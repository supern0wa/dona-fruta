import Vue from 'vue'
import moment from 'moment'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		orders: {},
		current: {},
		daily: {},
		readOnly: false,
		generated: false,
		week: null
	},
	getters: {
		getWeek (state) {
			return state.week
		},
		getCurrent (state) {
			return state.current
		},
		getOrders (state) {
			return state.orders
		},
		getReadOnly (state) {
			return state.readOnly
		},
		getDaily (state) {
			return state.daily
		}
	},
	actions: {
		fetchOrder ({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/orders/week/${payload.week}`)
					.then(response => {
						if (response.status == 200) {
							// Prevent going further in future
							if (state.generated && response.body.orders.length == 0 && payload.week > moment.today().week() + 1) {
								store.commit('logger/error', {
									message: 'Por favor, confirme a semana atual para prosseguir.'
								})
							} else {
								// Update week
								commit('setWeek', payload.week)

								if (response.body.orders.length == 0)
									commit('generateCurrent')
								else
									commit('formatCurrent', response.body.orders)

								return resolve(state.current)
							}
						}

						return reject(response)
					})
			})
		},
		fetchDaily ({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				if (Object.keys(state.daily).length > 0)
					return resolve(state.daily)

				Vue.http.get(`${process.env.API_URL}/v1/user/day-items`)
					.then(response => {
						if (response.status == 200 && Object.keys(response.body.items).length > 0)
							state.daily = response.body.items

						return resolve(state.daily)
					})
			})
		},
		create ({ state, commit }, payload) {
			store.commit('logger/info', {
				message: 'Aguarde, estamos processando seu pedido...'
			})

			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/v1/orders`, {
					orders: payload.orders
				})
					.then(response => {
						commit('formatCurrent', response.body.orders)

						store.commit('logger/info', {
							message: 'Pedido cadastrado com sucesso.'
						})

						return resolve(response)
					}, error => store.commit('logger/error', { message: error.body.error }))
			})
		},
		update ({ state, commit }, payload) {
			store.commit('logger/info', {
				message: 'Aguarde, estamos atualizando seu pedido...'
			})

			return new Promise((resolve, reject) => {
				Vue.http.patch(`${process.env.API_URL}/v1/orders`, {
					orders: payload.orders,
					revision: payload.revision.id
				})
					.then(response => {
						console.log(response)

						if (response.status == 200) {
							payload.revision.status = 1

							store.commit('logger/info', {
								message: 'Pedido atualizado com sucesso.'
							})

							return resolve(response)
						}
					}, error => store.commit('logger/error', { message: error.body.error }))
			})
		}
	},
	mutations: {
		setWeek (state, value) {
			state.week = value
		},
		formatCurrent (state, orders) {
			let products = store.state.menu.products,
				items = {}

			// Reset current
			state.current = {}
			state.generated = false
			state.readOnly = true

			// Populate current order
			for (let index in orders) {
                // Populate items
                for (let index in products) {
                    let product = products[index]
                    items[product.id] = 0
                }
                
				let order = orders[index]

				order.menuproducts.forEach(menu => {
					items[menu.id] = menu.pivot.quantity
				})

				state.current[order.date] = Object.assign({}, items)
			}
		},
		generateCurrent (state) {
			let week = state.week,
				date = moment(week, 'w').weekday(0),
				products = store.state.menu.products,
				scheduler = store.state.settings.scheduler,
				items = {}

			// Reset current
			state.current = {}
			state.generated = true
			state.readOnly = moment().week() > week && scheduler.lockOnEmpty

			// Populate items
			for (let index in products) {
				let product = products[index]
				items[product.id] = 0
			}

			// Populate current orders
			for (let i = 1; i <= 7; i++) {
				state.current[date.format('YYYY-MM-DD')] = Object.assign({}, items)
				// Increment date
				date.add(1, 'days')
			}
		}
	}
}
