import Vue from 'vue'
import moment from 'moment'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		revisions: []
	},
	getters: {
		getRevisions (state) {
			return state.revisions
		}
	},
	mutations: {
		setRevisions (state, revisions) {
			state.revisions = revisions
		}
	},
	actions: {
		fetchRevisions ({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/revisions`)
					.then(response => {
						if (response.status == 200) {
							state.revisions = response.body.revisions

							return resolve(state.revisions)
						}

						return reject(response.body.error)
					}, error => reject(error.body.error))
			})
		},
		createRevision ({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/v1/revisions`,
					{
						user: payload.id,
						initial: payload.start,
						final: payload.end
					})
					.then(response => {
						if (response.status == 200) {
							store.commit('logger/info', {
								message: response.body.message
							})

							return resolve(response.body.revisions)
						}

						return reject(response.body.error)
					}, error => reject(error.body.error))
			})
		},
		updateRevision ({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.patch(`${process.env.API_URL}/v1/revisions/${payload.id}`)
				.then(response => {
					if (response.status == 200) {
						return resolve(response)
					}

					return reject(response.body.error)
				}, error => reject(error.body.error))
			})
		}
	}
}
