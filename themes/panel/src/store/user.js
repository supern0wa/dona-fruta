import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,
	state: {
		users: []
	},
	getters: {
		getUsers (state) {
			return state.users
		}
	},
	mutations: {
		updateUsers (state, payload) {
			state.users = payload
		}
	},
	actions: {
		fetchUsers ({ state }, payload) {
			return new Promise((resolve, reject) => {
				if (state.users.length > 0)
					return resolve(state.users)

				Vue.http.get(`${process.env.API_URL}/v1/users`)
					.then(response => {
						if (response.status == 200) {
							state.users = response.body.users

							return resolve(state.users)
						}

						return reject(state.users)
					}, error => console.error(error))
			})
		},

		getUser ({ dispatch, state }, payload) {
			return new Promise((resolve, reject) => {
				if (state.users.length == 0) {
					dispatch('fetchUsers')
						.then(users => {
							users.forEach(user => {
								if (user.id == payload.id)
									return resolve(user)
							})

							return reject()
						})
				} else {
					state.users.forEach(user => {
						if (user.id == payload.id)
							return resolve(user)
					})

					return reject()
				}
			})
		},

		getRevisions ({ dispatch, state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/users/${payload.id}/revisions`)
					.then(response => {
						if (response.status == 200)
							return resolve(response.body.revisions)

						return reject(response)
					}, error => console.error(error))
			})
		},

		createUser ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/v1/users/create`,
					Object.assign({
						password: payload.password.first,
						password_confirmation: payload.password.second
					}, payload.user)
				)
					.then(response => {
						if (response.status == 200) {
							state.users.unshift(response.body.user)

							store.commit('logger/info', {
								message: 'Usuário criado com sucesso'
							})

							return resolve(response.body.user)
						}

						return reject(response)
					}, error => {
						store.commit('logger/error', {
							message: error.body.error
						})

						return reject(error)
					})
			})
		},
		updateUser ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.patch(`${process.env.API_URL}/v1/users/update/${payload.user.id}`,
					Object.assign({
						password: payload.password.first,
						password_confirmation: payload.password.second
					}, payload.user)
				)
					.then(response => {
						if (response.status == 200) {
							store.commit('logger/info', {
								message: 'Usuário editado com sucesso'
							})

							// Update users array
							state.users.forEach((user, index) => {
								if (user.id == response.body.user.id)
									state.users[index] = response.body.user
							})

							return resolve(response.body)
						}

						return reject(response)
					}, error => {
						store.commit('logger/error', {
							message: error.body.error
						})

						return reject(error)
					})
			})
		},
		toggleBlock ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/v1/users/block/${payload.id}`)
					.then(response => {
						if (response.status == 200) {
							state.users.forEach((user, index) => {
								if (user.id == response.body.user.id)
									state.users[index].status = response.body.user.status
							})

							store.commit('logger/info', {
								message: `Usuário ${response.body.user.status == 0 ? 'bloqueado' : 'desbloqueado'} com sucesso`
							})

							return resolve(response.body.user.status)
						}

						return reject(response)
					})
			})
		},
		getWallet ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/users/${payload.id}/wallet/${payload.month}`)
					.then(response => {
						if (response.status == 200) {
							return resolve(response.body.wallet)
						}

						return reject(response)
					})
			})
		},
		getOrders ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/users/orders/${payload.id}/${payload.week}`)
					.then(response => {
						if (response.status == 200) {
							let products = store.state.menu.products,
								items = {},
								orders = {}

							// Populate current order
							for (let index in response.body.orders) {
								// Populate items
								for (let index in products) {
									let product = products[index]
									items[product.id] = 0
								}

								let order = response.body.orders[index]

								order.menuproducts.forEach(menu => {
									items[menu.id] = menu.pivot.quantity
								})

								orders[order.date] = Object.assign({}, items)
							}

							return resolve(orders)
						}

						return reject(response)
					})
			})
		}
	}
}
