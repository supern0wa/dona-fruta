import Vue from 'vue'
import store from '@/store'
import moment from 'moment'

export default {
	namespaced: true,
	state: {
		user: {},
		wallet: {},
		company: {},
		pending: [],
		refreshing: false,
		expires_in: 60
	},
	getters: {
		getUser (state) {
			return state.user
		},
		getWallet (state) {
			return state.wallet
		},
		getCompany (state) {
			return state.company
		},
		getPending (state) {
			return state.pending
		}
	},
	actions: {
		fetchUser ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/v1/user`)
					.then(response => {
						if (response.status == 200) {
							state.user = response.body.user
							state.wallet = response.body.wallet
							state.company = state.user.company

							return resolve(state.user)
						}

						return reject(response)
					}, error => console.error(error))
			})
		},
		refresh ({ commit, state }, token) {
			// Start refreshing
			state.refreshing = true

			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/refresh`, {
					token,
					refresh: true
				})
					.then(response => {
						// Stop refreshing
						state.refreshing = false

						if (response.status == 200) {
							let token = response.body.token

							// Set token
							commit('setToken', token)

							// Emit event to body element
							document.body.dispatchEvent(new CustomEvent('token-refreshed', {
								detail: token
							}))

							return resolve(token)
						}

						return reject(response)
					}, error => console.error(error))
			})
		},
		login ({ commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/login`, {
					email: payload.email,
					password: payload.password
				})
					.then(response => {
						if (response.status == 200) {
							let token = response.body.token

							commit('setToken', token)
							return resolve(token)
						}

						return reject(response)
					}, error => {
						if (error.status == 401)
							store.commit('logger/error', {
								message: 'Login ou senha inválido.'
							})
						else if (error.status == 403)
							store.commit('logger/error', {
								message: 'Usuário inativo, contate o responsável'
							})

						return reject(error)
					})
			})
		},
		logout ({ commit, state }, payload) {
			// Reset application states
			state.user = {}
			state.wallet = {}
			state.company = {}
			state.pending = []
			store.state.order.orders = {}
			store.state.order.current = {}
			store.state.order.daily = {}
			store.state.order.week = null
			store.state.user.users = []
			store.state.menu.menu = {}
			store.state.menu.products = []
			store.state.menu.featured = []
			store.state.menu.categories = []

			return new Promise((resolve, reject) => {
				commit('removeToken')

				return resolve()
			})
		},
		resetPassword ({ commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/v1/user/reset`, {
					current: payload.current,
					password: payload.first,
					confirmation: payload.second
				})
					.then(response => {
						if (response.status == 200) {
							store.commit('logger/info', {
								message: 'Senha alterada com sucesso.'
							})

							return resolve(response)
						}

						return reject(response)
					}, error => {
						store.commit('logger/error', {
							message: error.body.message
						})

						return reject(error)
					})
			})
		},
		fetchPending ({ state }, payload) {
			return new Promise((resolve, reject) => {
				if (state.pending.length > 0)
					return resolve(state.pending)

				Vue.http.get(`${process.env.API_URL}/v1/user/pending`)
					.then(response => {
						console.log(response)
						if (response.status == 200) {
							state.pending = response.body.orders

							return resolve(state.pending)
						}

						return reject(response)
					})
			})
		}
	},
	mutations: {
		setToken (state, token) {
			let expires_at = moment().add(state.expires_in, 'minute').format('x')

			localStorage.setItem('token', token)
			localStorage.setItem('expires_at', expires_at)
		},
		removeToken (state) {
			localStorage.removeItem('token')
			localStorage.removeItem('expires_at')
		}
	}
}
