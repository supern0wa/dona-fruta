// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import VueMaterial from 'vue-material'
import Vuelidate from 'vuelidate'
import Router from 'vue-router'
import NProgress from 'nprogress'
import Shared from './components/shared'
import Filters from './filters'
import Directives from './directives'
import moment from 'moment'
import 'vue-material/dist/vue-material.min.css'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueResource)
Vue.use(VueMaterial)
Vue.use(Vuelidate)
Vue.use(Router)
Vue.use(Shared)
Vue.use(Filters)
Vue.use(Directives)

moment.locale('pt-br')

Vue.http.interceptors.push((request, next) => {
	NProgress.start()
	next(response => {
		NProgress.done()
		return response
	})
})

/* eslint-disable no-new */
new Vue({
	el: '#app',
	store: require('./store').default,
	router: require('./router').default,
	components: { App: require('./App').default },
	template: '<App/>'
})
