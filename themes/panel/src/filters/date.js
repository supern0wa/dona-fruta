import moment from 'moment'

export default (value, format) => {
	return moment(value).format(format || 'DD/MM/YYYY hh:mm')
}
