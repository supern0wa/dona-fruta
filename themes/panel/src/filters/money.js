import accounting from 'accounting'

function isNumeric (n) {
	return !isNaN(parseFloat(n)) && isFinite(n)
}

export default (value) => {
	let money = value

	// Check if is numeric
	if (isNumeric(money))
		money = money.toString()

	// Fix dots
	money = money.replace(',', '')
		.replace(',', '.')

	// Format value
	return accounting.formatMoney(money, {
		symbol: 'R$',
		decimal: ',',
		thousand: '.',
		precision: 2,
		format: '%s %v'
	})
}
