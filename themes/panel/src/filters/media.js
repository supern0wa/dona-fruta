export default (value) => {
	return `${process.env.API_URL}/v1/uploads/${value}`
}
