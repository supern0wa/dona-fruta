import Money from './money';
import Media from './media';
import Capitalize from './capitalize';
import Date from './date';

export default function install(Vue) {
	Vue.filter('money', Money);
	Vue.filter('media', Media);
	Vue.filter('capitalize', Capitalize);
	Vue.filter('date', Date);
}
