import VMasker from 'vanilla-masker'
import Cleave from 'cleave.js'

export default {
	bind: function (el, binding, vnode) {
		if (binding.arg == 'money')
			el.mask = new Cleave(el, {
				numeral: true,
				numeralDecimalMark: ',',
				delimiter: '.'
			})
	},
	unbind: function (el, binding, vnode) {
		el.mask.destroy()
	}
}
