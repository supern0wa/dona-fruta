import Mask from './mask'
import { VMoney } from 'v-money'

export default function install(Vue) {
	Vue.directive('mask', Mask)
	Vue.directive('money', VMoney)
}
