import InputNumber from './input/InputNumber'

export default function install(Vue) {
	Vue.component('df-input-number', InputNumber)
}
