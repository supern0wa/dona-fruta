import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

// Components
import Auth from '@/components/auth/Auth'
import Dashboard from '@/components/dashboard/Dashboard'
import Home from '@/components/home/Home'
import OrderWeekly from '@/components/order/OrderWeekly'
import OrderPending from '@/components/order/OrderPending'
import UserList from '@/components/user/UserList'
import UserForm from '@/components/user/UserForm'
import MenuProducts from '@/components/menu/MenuProducts'
import Settings from '@/components/settings/Settings'

let router = new Router({
	// Application routes
	routes: [
		{
			path: '/',
			redirect: '/auth'
		},
		{
			path: '/auth',
			component: Auth,
			meta: {
				public: true
			}
		},
		{
			path: '/dashboard',
			name: 'Dashboard',
			component: Dashboard,
			children: [
				{
					path: '/',
					name: 'Home',
					component: Home
				},
				{
					path: '/order/weekly',
					name: 'OrderWeekly',
					component: OrderWeekly
				},
				{
					path: '/order/pending',
					name: 'OrderPending',
					component: OrderPending
				},
				{
					path: '/users',
					name: 'Users',
					component: UserList,
					meta: {
						restricted: true
					}
				},
				{
					path: '/users/edit/:id',
					name: 'UserEdit',
					component: UserForm,
					meta: {
						restricted: true,
						method: 'edit'
					}
				},
				{
					path: '/users/create',
					name: 'UserCreate',
					component: UserForm,
					meta: {
						restricted: true,
						method: 'create'
					}
				},
				{
					path: '/menu',
					name: 'Menu',
					component: MenuProducts,
					meta: {
						restricted: true
					}
				},
				{
					path: '/settings',
					name: 'Settings',
					component: Settings
				}
			]
		},
	]
})

router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.public)) {
		next()
	} else {
		if (localStorage.getItem('token'))
			if (to.matched.some(record => record.meta.restricted)) {
				if (!store.state.auth.user.permission) {
					store.dispatch('auth/fetchUser')
						.then(user => {
							if (user.permission == 'Responsavel') {
								next()
							} else {
								store.commit('logger/error', { message: 'Área restrita!' })
								next({ path: '/' })
							}
						})
				} else if (store.state.auth.user.permission == 'Responsavel') {
					next()
				} else {
					store.commit('logger/error', { message: 'Área restrita!' })
					next({ path: '/' })
				}
			} else {
				next()
			}
		else
			next({
				path: '/'
			})
	}
})

export default router
