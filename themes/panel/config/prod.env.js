'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API_URL: process.env.PROD_API ? `"${process.env.PROD_API}"` : '"http://dona-fruta.herokuapp.com/api"',
	COMPANY_NAME: process.env.COMPANY_NAME ? `"${process.env.COMPANY_NAME}"` : '"Dona Fruta"',
	COMPANY_BRAND: process.env.COMPANY_BRAND ? `"${process.env.COMPANY_BRAND}"` : '"dona-fruta-brand.png"'
}
