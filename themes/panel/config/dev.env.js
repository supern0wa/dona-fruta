'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: process.env.DEV_API ? `"${process.env.DEV_API}"` : '"http://dona-fruta.local/api"',
	COMPANY_NAME: process.env.COMPANY_NAME ? `"${process.env.COMPANY_NAME}"` : '"Dona Fruta"',
	COMPANY_BRAND: process.env.COMPANY_BRAND ? `"${process.env.COMPANY_BRAND}"` : '"dona-fruta-brand.png"'
})
