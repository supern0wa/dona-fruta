<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeMenuProducts extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_menu_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('show')->default(1);
            $table->string('diff_value')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_menu_products');
    }
}
