<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeProductsOrder extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_products_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('menu_product_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('quantity');
            $table->decimal('current_value', 10, 2)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_products_order');
    }
}
