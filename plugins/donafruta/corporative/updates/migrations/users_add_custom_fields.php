<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersAddCustomFields extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {   
            $table->string('credit')->nullable();
            $table->string('permission')->default('Colaborador')->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->smallInteger('status')->default(1)->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
//            $table->dropColumn('credit');
//            $table->dropColumn('permission');
//            $table->dropColumn('company_id');
//            $table->dropColumn('status');
        });
    }
}
