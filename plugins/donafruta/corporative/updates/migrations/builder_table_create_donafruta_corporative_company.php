<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeCompany extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_company', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('address_id')->unsigned()->index()->nullable();
            $table->string('name');
            $table->string('cnpj')->unique();
            $table->date('included')->nullable();
            $table->string('deliver_tax')->nullable();
            $table->string('credit')->nullable();
            $table->smallInteger('status');
            $table->string('tipo_pagamento');
            $table->string('thumbnail')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_company');
    }
}
