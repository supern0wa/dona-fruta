<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeWalletsAudiction extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_wallets_audiction', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('responsable_id')->unsigned();
            $table->double('old_credit', 10, 0);
            $table->double('new_credit', 10, 0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_wallets_audiction');
    }
}
