<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeMenu extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_menu', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('company_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_menu');
    }
}
