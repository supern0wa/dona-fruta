<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeProducts extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('thumbnail')->nullable();
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->string('base_value');
            $table->text('description')->nullable();
            $table->string('calories')->nullable();
            $table->smallInteger('featured');
            $table->smallInteger('suspended');
            $table->string('barcode')->unique();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_products');
    }
}