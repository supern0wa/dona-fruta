<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeAddress extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_address', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('street');
            $table->text('number');
            $table->text('neighbourhood');
            $table->text('city');
            $table->text('state');
            $table->text('complement');
            $table->text('zipcode');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_address');
    }
}
