<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeRevisions extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_revisions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('responsable_id')->unsigned();
            $table->integer('funcionario_id')->unsigned();
            $table->date('initial_date');
            $table->date('final_date');
            $table->integer('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_revisions');
    }
}
