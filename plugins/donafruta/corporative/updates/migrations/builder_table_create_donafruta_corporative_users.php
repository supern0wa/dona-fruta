<?php namespace DonaFruta\Corporative\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDonafrutaCorporativeUsers extends Migration
{
    public function up()
    {
        Schema::create('donafruta_corporative_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->decimal('credit', 10, 2);
            $table->string('role');
            $table->smallInteger('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('donafruta_corporative_users');
    }
}
