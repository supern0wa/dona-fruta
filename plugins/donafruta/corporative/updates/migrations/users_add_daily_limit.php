<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersAddDailyLimit extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {   
            $table->string('daily_limit')->default(0)->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('daily_limit');
        });
    }
}
