<?php namespace DonaFruta\Corporative\Updates;

//use Seeder;
use October\Rain\Database\Updates\Seeder;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\Company;

class SeedUserTable extends Seeder
{
    public function run()
    {
        if (env('APP_ENV') != 'production') {
            $faker = \Faker\Factory::create();
            $companies = Company::all()->lists('id'); 
           
            for($i = 0; $i <= 15; $i++){
                $k2 = array_rand($companies,1);
                $user = new User([
                    'company_id'            => $companies[$k2],
                    'name'                  => $faker->text(10),
                    'email'                 => $faker->email(),
                    'password'              => $faker->password(),
                    'credit'                => '600.00',
                    'permission'            => $faker->randomElement(['Responsavel', 'Colaborador']),
                    'status'                => 1
                ]);
                $user->forceSave();
            }
        }
    }
}
