<?php namespace DonaFruta\Corporative\Updates;

//use Seeder;
use October\Rain\Database\Updates\Seeder;
use DonaFruta\Corporative\Models\Product;
use DonaFruta\Corporative\Models\ProductCategory;

class SeedProductsTable extends Seeder
{
	public function run()
	{
		if (env('APP_ENV') != 'production') {
			$faker = \Faker\Factory::create();
			$categories = [];
			for($i=0; $i<4; $i++){
				$categories[] = [
					'name' => $faker->randomElement([
						'Frutas Fatiadas', 'Acaís', 'Sanduíches', 'Sucos', 'Saladas',
						'Almoço Fitness', 'Almoço Gourmet', 'Frutas In Natura',
						'Tapiocas', 'Pasteis Natural','Frutas Fatiadas2', 'Acaís3', 'Sanduíches4', 'Sucos5', 'Saladas6',
						'Almoço Fisadtness7', 'Aasdasdlmoço Gourmet8', 'Frutasdasdas In Natura1',
						'Tapiocasdas32', 'Pasteis Natural423','Almoço Fitnessdsd', 'Almoço Gourmetsdsds', 'Frutas asdsadIn Natura',
						'Tapioasdcas', 'Pasteis Naasdasdtural','Frutas Fatiaasdasddas2', 'Acaasasddís3', 'Sansasddaduíches4', 'Suasdascos5', 'Saladasdasdas6',
						'Almoçasdo Fitness7', 'Almoçasdasdo Gourmet8', 'Frutas In Natura1',
						'Tapiocsadasdas32', 'Pasteis Naturdasdal423',
					])
				];
			}
			ProductCategory::insert($categories);
			$categories = ProductCategory::all()->lists('id');

			for($i = 0; $i <= 15; $i++){
				$k = array_rand($categories,1);
				$product = Product::create([
					'name'                  => $faker->colorName(),
					'thumbnail'             => NULL,
					'category_id'           => $categories[$k],
					'base_value'            => 5.00,
					'description'           => $faker->sentence(4),
                    'featured'              => 0,
                    'suspended'             => 0,
                    'barcode'               => $faker->numberBetween(1111111111111,9999999999999),
					'calories'              => $faker->randomNumber(3)
				]);
			}
		}
	}
}
