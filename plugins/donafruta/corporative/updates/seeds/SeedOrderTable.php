<?php namespace DonaFruta\Corporative\Updates;

//use Seeder;
use October\Rain\Database\Updates\Seeder;
use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\User;
use \Carbon\Carbon;

class SeedOrderTable extends Seeder
{
    public function run()
    {
        if (env('APP_ENV') != 'production') {
            $faker = \Faker\Factory::create();
            $users = User::all()->lists('id');
            $menu = Menu::where('company_id', 1)->first();
            
            $menuProducts = MenuProduct::where('menu_id', $menu->id)->get();
            
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
           
            $firstDay = Carbon::now()->startOfWeek();
            $lastDay = Carbon::now()->endOfWeek();
            
            while($firstDay->toDateString() != $lastDay->toDateString()){
                $k = array_rand($users,1);
                $order = Order::create([
                    'user_id'               => 1,
                    'date'                  => $firstDay->toDateString(),
                    'status'                => 1
                ]);
                for($i=0; $i <= 3; $i++){
                    $order->menuproducts()->add($menuProducts->random(), null, ['quantity' => 3]);
                }
                $firstDay->addDay();
            }
        }
    }
}
