<?php namespace DonaFruta\Corporative\Updates;

//use Seeder;
use October\Rain\Database\Updates\Seeder;
use DonaFruta\Corporative\Models\Product;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\MenuProduct;
use \Carbon\Carbon;

class SeedMenuTable extends Seeder
{
    public function run()
    {
        if (env('APP_ENV') != 'production') {
            $companies = Company::all()->lists('id'); 
            $products = Product::all()->lists('id');

            foreach($companies as $company){
                 $menu = Menu::create([
                    'company_id' => $company
                ]);
            }
            $menus = Menu::all()->lists('id');
            foreach($menus as $menu){
                $product_id = array_rand($products,1);
                foreach($products as $prod){
                    MenuProduct::create([
                       'menu_id' => $menu,
                       'product_id' => $prod,
                       'diff_value' => 5
                    ]);
                }
            }
        }
    }
}
