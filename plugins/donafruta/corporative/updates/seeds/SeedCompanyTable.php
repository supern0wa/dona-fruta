<?php

namespace DonaFruta\Corporative\Updates;

//use Seeder;
use October\Rain\Database\Updates\Seeder;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\Address;

class SeedCompanyTable extends Seeder {

    private static function mod($dividendo, $divisor) {
        return round($dividendo - (floor($dividendo / $divisor) * $divisor));
    }

    public static function cnpjRandom($mascara = "1") {
        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
        $n9 = 0;
        $n10 = 0;
        $n11 = 0;
        $n12 = 1;
        $d1 = $n12 * 2 + $n11 * 3 + $n10 * 4 + $n9 * 5 + $n8 * 6 + $n7 * 7 + $n6 * 8 + $n5 * 9 + $n4 * 2 + $n3 * 3 + $n2 * 4 + $n1 * 5;
        $d1 = 11 - (self::mod($d1, 11) );
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n12 * 3 + $n11 * 4 + $n10 * 5 + $n9 * 6 + $n8 * 7 + $n7 * 8 + $n6 * 9 + $n5 * 2 + $n4 * 3 + $n3 * 4 + $n2 * 5 + $n1 * 6;
        $d2 = 11 - (self::mod($d2, 11) );
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . "." . $n3 . $n4 . $n5 . "." . $n6 . $n7 . $n8 . "/" . $n9 . $n10 . $n11 . $n12 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $n10 . $n11 . $n12 . $d1 . $d2;
        }
        return $retorno;
    }

    public function run() {
        if (env('APP_ENV') != 'production') {
            $faker = \Faker\Factory::create();
            $addresses = [];
            for ($i = 0; $i <= 10; $i++) {
                $addresses[] = [
                    'street' => $faker->streetAddress(),
                    'number' => $faker->randomNumber(3),
                    'neighbourhood' => $faker->city(),
                    'city' => $faker->city(),
                    'state' => $faker->state(),
                    'complement' => $faker->text(15),
                    'zipcode' => $faker->postcode(),
                ];
            }
            Address::insert($addresses);
            $addresses = Address::all()->lists('id');

            for ($i = 0; $i <= 15; $i++) {
                $k = array_rand($addresses, 1);
                $company = new Company([
                            'address_id' => $addresses[$k],
                            'name' => $faker->company(),
                            'cnpj' => $this->cnpjRandom(),
                            'included' => $faker->date('Y-m-d', 'now'),
                            'deliver_tax' => $faker->randomFloat(2, 5, 15),
                            'credit' => $faker->randomFloat(2, 5, 15),
                            'tipo_pagamento' => 'Total',
                            'status' => 1
                ]);
                $company->forceSave();
            }
        }
    }

}
