$(document).ready(function() {
	$('.report-company').each(function() {
		let $el = $(this),
			$select = $el.find('.report-company__select'),
			$on = $el.find('.report-company__on'),
			$off = $el.find('.report-company__off'),
			$box = $el.find('.report-company__box'),
			$wrapper = $el.find('.report-company__wrapper')

		this.render = function (users) {
			$box.html('')

			users.forEach(user => {
				$box.append(`
					<div class="checkbox custom-checkbox">
						<input type="checkbox" id="user_${user.id}" name="users[${user.id}]" />
						<label for="user_${user.id}">${user.name}</label>
					</div>
				`)
			})

			// Select all checkboxes
			$on.click()
		}

		$select.on('change', event => {
			let value = event.currentTarget.value

			if (!$wrapper)
				return

			if (value == 'all') {
				$wrapper.slideUp()
			} else {
				$.request('onChange', {
					data: {
						id: value
					},
					success: data => {
						this.render(data.users)
						$wrapper.slideDown()
					}
				})
			}
		})

		$on.on('click', event => {
			let $checkboxes = $el.find('.checkbox')
			$checkboxes.find('input').prop('checked', true)
		})

		$off.on('click', event => {
			let $checkboxes = $el.find('.checkbox')
			$checkboxes.find('input').prop('checked', false)
		})
	})
})
