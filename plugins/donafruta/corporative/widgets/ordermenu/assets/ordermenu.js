$(document).ready(function() {
	// TODO: We need to fetch new order data 
	// when the date is changed

	// TODO: We need to recreate event observers
	// when the date is changed

	$('.order-menu').each(function() {
		let $element = $(this)

		$element.find('.order-menu__input').each(function() {
			let $input = $(this)

			$input.on('keydown', e => {
				let value = parseInt(e.target.value)

				switch (e.keyCode) {
					case 38: // Up Arrow
						e.target.value = ++value
						break;
					case 40: // Down Arrow
						if (value - 1 >= 0)
							e.target.value = --value
						break;
					default:
						e.preventDefault()
				}
				console.log(e.keyCode)
			})
		})
	})
})
