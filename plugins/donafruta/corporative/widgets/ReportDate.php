<?php
namespace DonaFruta\Corporative\Widgets;

use Backend\Classes\WidgetBase;
use \Carbon\Carbon as CarbonDate;

class ReportDate extends WidgetBase
{
	/**
	 * @var string A unique alias to identify this widget.
	 */
	protected $defaultAlias = 'reportdate';

	public function __construct($controller, $company_id)
	{
		parent::__construct($controller);
		$this->addCss('/plugins/donafruta/corporative/widgets/reportdate/assets/reportdate.css');
		$this->addJs('/plugins/donafruta/corporative/widgets/reportdate/assets/reportdate.js');
	}

	public function render($active = 0, $showButtons = [])
	{
		return $this->makePartial('component', [
			'active' => $active,
			'buttons' => $showButtons
		]);
	}
}
