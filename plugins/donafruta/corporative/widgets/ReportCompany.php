<?php
namespace DonaFruta\Corporative\Widgets;

use Backend\Classes\WidgetBase;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\User;

class ReportCompany extends WidgetBase
{
	/**
	 * @var string A unique alias to identify this widget.
	 */
	protected $defaultAlias = 'reportcompany';

	public function __construct($controller)
	{
		parent::__construct($controller);
		$this->addCss('/plugins/donafruta/corporative/widgets/reportcompany/assets/reportcompany.css');
		$this->addJs('/plugins/donafruta/corporative/widgets/reportcompany/assets/reportcompany.js');
	}

	public function render($showUsers = true)
	{
		$companies = Company::all();

		return $this->makePartial('component', [
			'companies' => $companies,
			'showUsers' => $showUsers
		]);
	}

	public function onChange()
	{
		$id = $_POST['id'];
		$users = User::where('company_id', $id)
			->where('status', 1)
			->orderBy('name')
			->get();

		return ['users' => $users];
	}
}
