<?php
namespace DonaFruta\Corporative\Widgets;

use Backend\Classes\WidgetBase;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Menu;

class CompanyMenu extends WidgetBase
{
	/**
	 * @var string A unique alias to identify this widget.
	 */
	protected $defaultAlias = 'companymenu';
	protected $company;

	public function __construct($controller, $company_id)
	{
		parent::__construct($controller);
		$this->addCss('/plugins/donafruta/corporative/widgets/companymenu/assets/css/companymenu.css');
		$this->addJs('/plugins/donafruta/corporative/widgets/companymenu/assets/js/companymenu.js');
		$this->company = Company::find($company_id);
	}

	public function render()
	{   
		$menu = Menu::where('company_id', $this->company->id)->first();
		$menuProducts = MenuProduct::where('menu_id', $menu->id)->get();
		$menuProducts = $menuProducts->sortBy('name');

		return $this->makePartial('list', ['menuProducts' => $menuProducts->toArray()]);
	}
}
