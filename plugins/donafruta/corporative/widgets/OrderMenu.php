<?php
namespace DonaFruta\Corporative\Widgets;

use Backend\Classes\WidgetBase;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\User;

class OrderMenu extends WidgetBase
{
	/**
	 * @var string A unique alias to identify this widget.
	 */
	protected $defaultAlias = 'ordermenu';
	protected $company;
	protected $user;

	// TODO: We need to fetch the data throught ajax requests

	public function __construct($controller, $user_id, $date)
	{
		parent::__construct($controller);

		$this->addCss('/plugins/donafruta/corporative/widgets/ordermenu/assets/ordermenu.css');
		$this->addJs('/plugins/donafruta/corporative/widgets/ordermenu/assets/ordermenu.js');

		$this->user = User::find($user_id);
	}

	public function render()
	{   
		$data = [];
		$menu = Menu::where('company_id', $this->user->company->id)->first();

		foreach ($menu->menuproducts as $product) {
			$data[$product->id] = [
				'id' => $product->id,
				'name' => $product->product->name,
				'category' => $product->product->category->name,
				'value' => $product->diff_value,
				'quant' => 0
			];
		}

		return $this->makePartial('list', ['products' => $data]);
	}

	public function onChange($date)
	{
		$order = Order::where('user_id', $this->user->id)
			->where('date', $date)
			->first();

		$quant = $product->pivot->quantity;

		foreach ($this->order->menuproducts as $product) {
			$price = $product->pivot->current_value;

			$data[$product->id] = [
				'id' => $product->id,
				'name' => $product->product->name,
				'category' => $product->product->category->name,
				'value' => $price,
				'quant' => 0
			];
		}

		return $data;
	}
}
