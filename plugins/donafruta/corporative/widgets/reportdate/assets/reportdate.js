$(document).ready(function() {
	$('.report-date').each(function() {
		let $el = $(this),
			$hidden = $el.find('input[type="hidden"]'),
			$range = $el.find('.report-date__range'),
			$start = $el.find('.report-date__start'),
			$end = $el.find('.report-date__end')

		// Date variables
		let today = moment(),
			tomorrow = moment().add(1, 'days'),
			week = moment().add(1, 'week'),
			startMonth = moment().startOf('month'),
			endMonth = moment().endOf('month')

		// Update datepicker
		this.setRange = function (start, end) {
			$start.find('input[type="text"]').val(start.format('DD/M/YYYY'))
			$start.find('input[type="hidden"]').val(start.format('YYYY-MM-DD'))
			$end.find('input[type="text"]').val(end.format('DD/M/YYYY'))
			$end.find('input[type="hidden"]').val(end.format('YYYY-MM-DD'))
		}

		$hidden.on('change', event => {
			let value = event.currentTarget.value

			// Hide range inputs
			$range.slideUp()

			switch (value) {
				case '0':
					this.setRange(today, today)
					break;
				case '1':
					this.setRange(tomorrow, tomorrow)
					break;
				case '7':
					this.setRange(today.clone().startOf('week'), today.clone().endOf('week'))
					break;
				case '14':
					this.setRange(week.clone().startOf('week'), week.clone().endOf('week'))
					break;
				case '30':
					this.setRange(startMonth, endMonth)
					break;
				case 'range':
					$range.slideDown()
					break;
			}
		})
	})
})
