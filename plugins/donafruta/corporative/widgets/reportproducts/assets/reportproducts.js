$(document).ready(function() {
	$('.report-products').each(function() {
		let $el = $(this),
			$category = $el.find('.report-products__category'),
			$checkboxes = $el.find('.checkbox'),
			$on = $el.find('.report-products__on'),
			$off = $el.find('.report-products__off'),
			$wrapper = $el.find('.report-products__wrapper')

		$category.on('change', event => {
			let value = event.currentTarget.value

			$checkboxes.find('input').prop('checked', false)

			if (value == 'all') {
				$checkboxes.show()
				$wrapper.slideUp()
			} else {
				let $targets = $el.find(`[data-category="${value}"]`)

				$checkboxes.hide()
				$targets.find('input').prop('checked', true)
				$targets.show()
				$wrapper.slideDown()
			}
		})

		$on.on('click', event => {
            let value = $category.val();
            let $targets = $el.find(`[data-category="${value}"]`);
            $targets.find('input').prop('checked', true);
		})

		$off.on('click', event => {
			$checkboxes.find('input').prop('checked', false)
		})
	})
})

