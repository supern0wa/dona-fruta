<?php
namespace DonaFruta\Corporative\Widgets;

use Backend\Classes\WidgetBase;
use DonaFruta\Corporative\Models\Product;
use DonaFruta\Corporative\Models\ProductCategory;

class ReportProducts extends WidgetBase
{
	/**
	 * @var string A unique alias to identify this widget.
	 */
	protected $defaultAlias = 'reportproducts';

	public function __construct($controller, $company_id)
	{
		parent::__construct($controller);
		$this->addCss('/plugins/donafruta/corporative/widgets/reportproducts/assets/reportproducts.css');
		$this->addJs('/plugins/donafruta/corporative/widgets/reportproducts/assets/reportproducts.js');
	}

	public function render()
	{
		$products = Product::all();
		$categories = ProductCategory::all();

		return $this->makePartial('component', ['products' => $products, 'categories' => $categories]);
	}
}
