$(document).ready(function(){
	$('[data-mask]').each(function() {
		$(this).mask(this.dataset.mask)
	})

	$('[data-mask-decimal]').each(function () {
		let value = $(this).val().replace(',', '.')

		value = parseFloat(value).toFixed(2)

		$(this).val(value)
	})

	$('[data-mask-decimal]').mask("#.##0,00", { reverse: true })
})
