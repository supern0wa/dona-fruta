<?php namespace DonaFruta\Corporative\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Widgets\OrderMenu;

/**
 * Scheduler Back-end Controller
 */
class Scheduler extends Controller
{
	public $implement = [
		'Backend\Behaviors\ListController'
	];
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		$this->addCss('/plugins/donafruta/corporative/controllers/scheduler/assets/scheduler.css');
		$this->addJs('/plugins/donafruta/corporative/controllers/scheduler/assets/scheduler.js');

		BackendMenu::setContext('DonaFruta.Corporative', 'scheduler');
	}

	public function show()
	{
		$data = [];
		$user = User::find($this->params[0]);
		$menu = Menu::where('company_id', $user->company->id)->first();

		foreach ($menu->menuproducts as $product) {
			$data[$product->id] = [
				'id' => $product->id,
				'name' => $product->product->name,
				'category' => $product->product->category->name,
				'value' => $product->diff_value,
				'quant' => 0
			];
		}

		$this->vars['user'] = $user->name;
		$this->vars['company'] = $user->company->name;
		$this->vars['products'] = $data;
	}

	public function onChange($date)
	{
		$data = [];
		$order = Order::where('user_id', $this->params[0])
			->where('date', $_POST['date'])
			->first();

		if ($order)
			foreach ($order->menuproducts as $product) {
				$quant = $product->pivot->quantity;
				$price = $product->pivot->current_value;

				$data[$product->id] = [
					'id' => $product->id,
					'name' => $product->product->name,
					'category' => $product->product->category->name,
					'value' => $price,
					'quant' => $quant
				];
			}

		return ['orders' => $data];
	}

	public function onSubmit()
	{
		if (!$_POST['day'])
			throw new \AjaxException('Campo data é obrigatório.');

		$data = [];
		$date = substr($_POST['day'], 0, 10);
		$quants = $_POST['quant'];

		$order = Order::where('user_id', $this->params[0])
			->where('date', $date)
			->first();

		// If there's no order this day, then create a new one
		if (!$order)
			$order = Order::create([
				'user_id' => $this->params[0],
				'date' => $date,
				'status' => 0
			]);

		foreach ($quants as $id => $quant) {
			if ((int)$quant > 0) {
				$product = MenuProduct::find($id);

				$data[$id] = [
					'quantity' => $quant,
					'current_value' => $product->diff_value 
				];
			}
		}

		$order->menuproducts()->sync($data);

		return ['order' => $order];
	}
}
