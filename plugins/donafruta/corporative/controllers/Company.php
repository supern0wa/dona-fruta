<?php

namespace DonaFruta\Corporative\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use DonaFruta\Corporative\Widgets\CompanyMenu;
use DonaFruta\Corporative\Models\Address;

class Company extends Controller
{
	public $implement = [
		'Backend\Behaviors\ListController',
		'Backend\Behaviors\FormController',
		'Backend\Behaviors\ReorderController',
		'Backend\Behaviors\RelationController'
	];

	public $listConfig = 'config_list.yaml';
	public $formConfig = 'config_form.yaml';
	public $reorderConfig = 'config_reorder.yaml';
	public $relationConfig = 'config_relation.yaml';

	public function __construct()
	{
		parent::__construct();
        if($this->params){
            $companyMenu = new CompanyMenu($this, $this->params[0]);
            $companyMenu->bindToController();
        }
	}

	public function formExtendFields($form)
	{
		$form->addFields([
			'address[street]' => [
				'label' => 'Rua',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-3'
			],
			'address[neighbourhood]' => [
				'label' => 'Bairro',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-3'
			],
			'address[number]' => [
				'label' => 'Número',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-1'
			],
			'address[city]' => [
				'label' => 'Cidade',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-3'
			],
			'address[state]' => [
				'label' => 'Estado',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-2'
			],
			'address[complement]' => [
				'label' => 'Complemento',
				'span' => 'storm',
				'type' => 'text',
				'tab' => 'Empresa',
				'cssClass' => 'col-sm-3'
			],
			'address[zipcode]' => [
				'label' => 'CEP',
				'span' => 'storm',
				'type' => 'text',
				'attributes' => [
					'data-mask' => "99999-999"
				],
				'tab' => 'Empresa',
				'required' => true,
				'cssClass' => 'col-sm-2'
			]
		], 'primary');
	}

	public function formExtendModel($model)
	{
		if (!$model->address)
			$model->address = new Address;

		return $model;
	}
}
