$(document).ready(function() {
	$('.emails--missing').each(function() {
		let $el = $(this),
			$form = $el.find('.form'),
			$list = $el.find('.list'),
			$table = $el.find('.control-list .table tbody'),
			$checkbox = $el.find('#checkboxAll'),
			$start = $list.find('.start'),
			$stop = $list.find('.stop'),
			$message = $el.find('.emails__textarea textarea'),
			stopped = true

		// Checkall feature
		$checkbox.on('input', function(event) {
			let = $checkboxes = $table.find('input[type="checkbox"]')

			$checkboxes.prop('checked', event.target.checked)
			$checkboxes.trigger('input')
		})

		// Listen to form submition
		$form.on('submit', function(event) {
			$list.slideUp()

			if ($form.find('select[name="company"]').val() == 'all')
				return $.oc.flashMsg({
					text: "Por favor, selecione uma empresa.",
					class: "error"
				})

			$form.request('onCompanyChange', {
				success: data => {
					let users = data.users

					if (users.length == 0)
						return $.oc.flashMsg({
							text: "Não existem pedidos em aberto neste período.",
							class: "error"
						})

					// Clear table body html
					$table.html('')
					$start.prop('disabled', false)

					// Loop through users
					users.forEach(function (user) {
						let html = `<tr>
							<td class="list-checkbox nolink">
								<div class="checkbox custom-checkbox nolabel">
									<input id="checkbox_${user.id}" type="checkbox" name="users[]" value="${user.id}" />
									<label for="checkbox_${user.id}">Check</label>
								</div>
							</td>
							<td>
								<span>
									${user.name}
								</span>
							</td>
							<td>
								<span>
									${user.email}
								</span>
							</td>
							<td>
								<span class="status oc-icon-circle text-muted">
									Pausado
								</span>
							</td>
						</tr>`

						// Append template to table
						$table.append(html)

						// Get added checkbox
						let = $checkboxes = $table.find('input[type="checkbox"]')

						// Listen to checkboxes event
						$checkboxes.on('input', function(event) {
							let $row = $(this).closest('tr')

							if (event.target.checked)
								$row.addClass('active')
							else
								$row.removeClass('active')
						})

						$checkbox.prop('checked', true)
						$checkbox.trigger('input')

						setTimeout(function() {
							$list.slideDown()
						}, 500)
					})
				}
			})

			event.preventDefault()
		})

		// Listen to send emails form submition
		$list.on('submit', function(event) {
			let users = [],
				current = 0,
				$checkboxes = $table.find('input[type="checkbox"]')

			$checkboxes.each(function(event) {
				let $checkbox = $(this)

				if ($checkbox.prop('checked'))
					users.push($checkbox.val())
			})

			event.preventDefault()

			if (users.length == 0)
				return $.oc.flashMsg({
					text: 'Nenhum usuário selecionado.',
					class: "error"
				})

			$start.prop('disabled', true)
			$stop.prop('disabled', false)

			// Change state
			stopped = false

			function sendNotification(index) {
				let $checkbox = $table.find('input[type="checkbox"][value="' + users[index] + '"]'),
					$row = $checkbox.closest('tr'),
					$status = $row.find('.status')

				$status.removeClass('text-muted')
				$status.addClass('text-info')
				$status.html('Enviando')

				$.request('onSendMissing', {
					data: {
						user: users[index],
						message: $message.val()
					},
					complete: data => {
						$status.removeClass('text-info')

						// If there are more users send to the next
						if (current + 1 < users.length && !stopped) {
							sendNotification(++current)
						} else {
							$start.prop('disabled', false)
							$stop.prop('disabled', true)
						}
					},
					success: data => {
						// Uncheck the row
						$checkbox.prop('checked', false)
						$checkbox.trigger('input')

						// Change status
						$status.removeClass('text-danger')
						$status.addClass('text-success')
						$status.html('Sucesso')

						$.oc.flashMsg({
							text: data.message,
							class: "success"
						})
					},
					error: data => {
						// Change status
						$status.removeClass('text-success')
						$status.addClass('text-danger')
						$status.html('Falhou')

						$.oc.flashMsg({
							text: data.responseJSON.message,
							class: "error"
						})
					}
				})
			}

			if (users.length > 0)
				sendNotification(current)
		})

		// Listen to stop button
		$stop.on('click', function(event) {
			$start.prop('disabled', false)
			$stop.prop('disabled', true)
			stopped = true

			event.preventDefault()
		})
	})
})
