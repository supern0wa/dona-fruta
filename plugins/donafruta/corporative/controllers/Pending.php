<?php namespace DonaFruta\Corporative\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\User;

class Pending extends Controller
{
	public $implement = [
		'Backend\Behaviors\ListController'
	];

	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();
	}

	public function show()
	{
		$user_id = $this->params[0];
		$this->vars['user'] = User::find($user_id)->name;

		// Non paid
		$nonPaidOrders = Order::nonPaidOrders($user_id);
		$nonPaidTotal = 0;

		foreach ($nonPaidOrders as $order)
			$nonPaidTotal += $order['total'];

		$this->vars['non_paid'] = [
			'orders' => $nonPaidOrders,
			'total' => $nonPaidTotal
		];

		// Paid
		$paidOrders = Order::paidOrders($user_id);
		$paidTotal = 0;

		foreach ($paidOrders as $order)
			$paidTotal += $order['total'];

		$this->vars['paid'] = [
			'orders' => $paidOrders,
			'total' => $paidTotal
		];
	}

	public function onChange($data)
	{
		$id = $_POST['order'];
		$checked = $_POST['value'];

		$order = Order::find($id);

		if ($order)
			$order->update([
				'paid' => $checked == "true"
			]);

		return [
			'paid' => $checked,
			'order' => $order,
			'total' => $order->getTotal()
		];
	}

	public function listExtendQuery($query)
	{
		$query->whereHas('company', function($query) {
			return $query->where('tipo_pagamento', 'individual');
		});
	}
}
