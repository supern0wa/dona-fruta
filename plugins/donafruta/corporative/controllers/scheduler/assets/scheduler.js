$(document).ready(function() {
	$('.scheduler').each(function() {
		let $el = $(this),
			$date = $el.find('.scheduler__day'),
			$menu = $el.find('.scheduler__menu'),
			$inputs = $el.find('.scheduler__input')

		$date.on('change', event => {
			let value = event.target.value

			$menu.slideUp()
			$.request('onChange', {
				data: {
					date: moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
				},
				success: data => {
					let orders = data.orders

					// Clear previous inputs values
					$inputs.val(0)
					$inputs.trigger('change')

					// Check if this date has order
					if (Object.keys(orders).length > 0)
						for (let id in orders) {
							let $input = $el.find(`input[name="quant[${id}]"]`),
								order = orders[id]

							$input.val(order.quant)
							$input.trigger('change')
						}
					else
						$inputs.each((index, el) => $(el).val(0))

					$menu.slideDown()
				},
				error: data => {
					$.oc.flashMsg({
						text: 'Nenhum pedido encontrado nesta data.',
						class: 'error'
					})
				}
			})
		})

		$inputs.each(function() {
			let $input = $(this)

			$input.on('keydown', e => {
				let value = parseInt(e.target.value)

				// Up key: Increase the value
				if (e.keyCode === 38) {
					$input.val(++value)
					$input.trigger('change')
					e.preventDefault()
					return
				}

				// Down key: Decrease the value
				if (e.keyCode === 40 && e.target.value - 1 >= 0) {
					$input.val(--value)
					$input.trigger('change')
					e.preventDefault()
					return
				}

				// Allow these keys only:
				if (
					// backspace, delete, tab, escape, enter
					[46, 8, 9, 27, 110].indexOf(e.keyCode) >= 0 ||
					// Ctrl/cmd+A
					(e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
					// Ctrl/cmd+C
					(e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
					// Ctrl/cmd+R
					(e.keyCode === 82 && (e.ctrlKey || e.metaKey)) ||
					// Ctrl/cmd+X
					(e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
					// home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)
				) {
					return
				}

				if (
					(e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
					(e.keyCode < 96 || e.keyCode > 105)
				) {
					e.preventDefault()
				}
			})

			// Toggle row active classes
			// When input change its value
			$input.on('change', e => {
				let value = +e.target.value,
					$row = $input.parent().parent().parent()

				if (value > 0) {
					$row.addClass('active')
				} else {
					$row.removeClass('active')
				}
			})
		})
	})
})
