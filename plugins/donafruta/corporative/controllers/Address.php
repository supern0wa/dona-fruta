<?php namespace DonaFruta\Corporative\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Address extends Controller
{
	public $implement = [
		'Backend\Behaviors\FormController'
	];

	public $formConfig = 'config_form.yaml';

	public function __construct()
	{
		parent::__construct();
	}
}
