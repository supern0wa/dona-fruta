<?php namespace DonaFruta\Corporative\Controllers;

use BackendMenu;
use Mail;
use Config;
use Backend\Classes\Controller;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Widgets\ReportDate;
use DonaFruta\Corporative\Widgets\ReportCompany;
use October\Rain\Exception\AjaxException;

/**
 * Emails Back-end Controller
 */
class Emails extends Controller
{
	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('DonaFruta.Corporative', 'main-menu-emails', 'side-menu-missing');

		$this->addCss('/plugins/donafruta/corporative/controllers/emails/assets/emails.css');
		$this->addJs('/plugins/donafruta/corporative/controllers/emails/assets/emails.js');

		$reportCompany = new ReportCompany($this, []);
		$reportCompany->bindToController();

		$reportDate = new ReportDate($this, []);
		$reportDate->bindToController();
	}

	public function missing()
	{
		//
	}

	public function onCompanyChange()
	{
		$filters = [
			'init' => $_POST['date-start'],
			'end' => $_POST['date-end'],
			'company' => $_POST['company'],
			'display' => 'all'
		];

		$users = Order::ausenciaPedidosReport($filters);

		return [
			'users' => $users
		];
	}

	public function onSendMissing() {
		try {
			$user = User::find($_POST['user']);

			// Send notification
			$sent = Mail::send('donafruta.corporative::mail.missing-order', [
				'name' => $user->name,
				'text' => $_POST['message'],
				'application' => Config::get('app.name')
			], function($message) use ($user) {
				$message->to($user->email, $user->name);
				$message->subject('Lembrete');
			});
		} catch (\Exception $e) {
			throw new AjaxException(['message' => $e->getMessage()]);
		}

		return ['message' => 'Email enviado para: ' . $user->email];
	}
}
