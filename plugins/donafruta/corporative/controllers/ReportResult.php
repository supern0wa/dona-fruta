<?php namespace DonaFruta\Corporative\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Helper;
use \Carbon\Carbon as CarbonDate;
use \Mpdf\Mpdf;

/**
 * Report Result Back-end Controller
 */
class ReportResult extends Controller
{
	public $layout = 'print';

	public function preparation()
	{
		$filters = [
			'init' => $_GET['date-start'],
			'end' => $_GET['date-end'],
		];

		if(isset($_GET['company']) && $_GET['company'] != 'all'){
			$filters['company'] = $_GET['company'];
		}

		if(isset($_GET['products']) && count($_GET['products']) > 0){
			foreach($_GET['products'] as $i => $v){
				$filters['products'][] = $i;
			}
		}

		$report = Order::ordemPreparacaoReport($filters);

		if ($_GET['company'] == 'all') {
			$company_name = 'Todas';
		} else {
			$company = Company::find($_GET['company']);
			$company_name = $company->name;
		}

		$todayDate = new CarbonDate();
		$separateInitDate = explode(' ', $filters['init']);
		$separateEndDate = explode(' ', $filters['end']);

		$initFormatDate = explode('-', $separateInitDate[0]);
		$endFormatDate = explode('-', $separateEndDate[0]);
		$filters['init'] = $initFormatDate[2] . "/" . $initFormatDate[1] . "/" . $initFormatDate[0];
		$filters['end'] = $endFormatDate[2] . "/" . $endFormatDate[1] . "/" . $endFormatDate[0];

		$data = ['report' => $report, 'filters' => $filters, 'date' => $todayDate->format('d/m/Y'), 'company_name' => $company_name];
		$this->vars['data'] = $data;
		;	}

		public function detail()
		{
			$filters = [
				'init' => $_GET['date-start'],
				'end' => $_GET['date-end'],
			];

			if(isset($_GET['company'])){
				$filters['company'] = $_GET['company'];

			}

			if(isset($_GET['users']) && $_GET['users'] != 'all'){
				$filters['users'] = $_GET['users'];
			}

			if(isset($_GET['products']) && count($_GET['products']) > 0){
				foreach($_GET['products'] as $i => $v){
					$filters['produts'][] = $i;
				}
			}

			// Get company and set company_name
			$company = Company::find($_GET['company']);

			if ($_GET['company'] == 'all')
				$company_name = 'Todas';
			else
				$company_name = $company->name;

			if (isset($_GET['missing-only'])) {
				$report = Order::ausenciaPedidosReport($filters);

				$todayDate = new CarbonDate();
				$separateInitDate = explode(' ', $filters['init']);
				$separateEndDate = explode(' ', $filters['end']);

				$initFormatDate = explode('-', $separateInitDate[0]);
				$endFormatDate = explode('-', $separateEndDate[0]);
				$filters['init'] = $initFormatDate[2] . "/" . $initFormatDate[1] . "/" . $initFormatDate[0];
				$filters['end'] = $endFormatDate[2] . "/" . $endFormatDate[1] . "/" . $endFormatDate[0];

				$data = ['report' => $report, 'filters' => $filters, 'date' => $todayDate->format('d/m/Y'), 'company_name' => $company_name];
			} else {
				$report = Order::detalhadoPedidosReport($filters);
				$formatedReport = [];
				foreach($report as $rep){
					$formatedReport[$rep->colaborador][] = $rep; 
				}

				$todayDate = new CarbonDate();
				$separateInitDate = explode(' ', $filters['init']);
				$separateEndDate = explode(' ', $filters['end']);

				$initFormatDate = explode('-', $separateInitDate[0]);
				$endFormatDate = explode('-', $separateEndDate[0]);
				$filters['init'] = $initFormatDate[2] . "/" . $initFormatDate[1] . "/" . $initFormatDate[0];
				$filters['end'] = $endFormatDate[2] . "/" . $endFormatDate[1] . "/" . $endFormatDate[0];

				$data = ['report' => $formatedReport, 'filters' => $filters, 'date' => $todayDate->format('d/m/Y'), 'company_name' => $company_name];
			}

			$this->vars['missingOnly'] = isset($_GET['missing-only']);
			$this->vars['data'] = $data;
	}

	public function missOrder($start, $end, $users)
	{
		$filters = [
			'init' => $_GET['date-start'],
			'end' => $_GET['date-end'],
		];

		if(isset($_GET['users']) && $_GET['users'] != 'all'){
			$filters['users'] = $_GET['users'];
		}

	}



	public function management()
	{
		$filters = [
			'init' => $_GET['date-start'],
			'end' => $_GET['date-end'],
		];

		if(isset($_GET['category']) && $_GET['category'] != 'all'){
			$filters['category'] = $_GET['category'];
		}

		if(isset($_GET['products']) && count($_GET['products']) > 0){
			foreach($_GET['products'] as $i => $v){
				$filters['products'][] = $i;
			}
		}

		$report = Order::gerencialPedidosReport($filters);
		$formatedReport = [];
		foreach($report as $rep){
			$formatedReport[$rep->category_name][] = $rep; 
		}
		$todayDate = new CarbonDate();
		$separateInitDate = explode(' ', $filters['init']);
		$separateEndDate = explode(' ', $filters['end']);

		$initFormatDate = explode('-', $separateInitDate[0]);
		$endFormatDate = explode('-', $separateEndDate[0]);
		$filters['init'] = $initFormatDate[2] . "/" . $initFormatDate[1] . "/" . $initFormatDate[0];
		$filters['end'] = $endFormatDate[2] . "/" . $endFormatDate[1] . "/" . $endFormatDate[0];

		$data = ['report' => $formatedReport, 'filters' => $filters, 'date' => $todayDate->format('d/m/Y')];
		$this->vars['data'] = $data;
	}

	public function financial()
	{
		$report = [];
		$formatInitDate = explode(' ', $_GET['date-start']);
		$formatEndDate = explode(' ', $_GET['date-end']);
		$pendingOnly = isset($_GET['pending-orders']);
		$range = [$formatInitDate[0], $formatEndDate[0]];

		if ($_GET['company'] == 'all') {
			$company_name = 'Todas';

			$companies = Company::orderBy('name');

			if ($pendingOnly)
				$companies = $companies->where('tipo_pagamento', 'individual');

			$companies = $companies->get();

			foreach ($companies as $company) {
				$total = 0;

				// Get orders from company
				$orders = Order::whereHas('user', function($query) use ($company) {
					return $query->where('company_id', $company->id);
				});

				// Filter orders by date range
				$orders = $orders->whereBetween('date', $range);

				// Show only pending orders
				if ($pendingOnly)
					$orders = $orders->where('paid', 0);

				// Order and retrieve content
				$orders = $orders->orderBy('date', 'ASC')->get();

				// Sum total
				foreach ($orders as $order)
					$total += $order->getTotal();

				$report[$company->name] = $total;
			}
		} else {
			$company = Company::find($_GET['company']);
			$company_name = $company->name;

			// Get selected users from company
			if (isset($_GET['users']))
				$users = $company->user()
					->whereIn('id', array_keys($_GET['users']))
					->orderBy('name')
					->get();
			else
				$users = $company->user;

			// Format report data
			foreach ($users as $user) {
				$report[$user->name] = [];

				// Filter orders by date range
				$orders = $user->orders()
					->whereBetween('date', $range);

				// Show only pending orders
				if ($pendingOnly)
					$orders = $orders->where('paid', 0);

				// Order data and retrieve content
				$orders = $orders->orderBy('date', 'ASC')->get();

				foreach ($orders as $order) {
					// Only print if total is not zero
					if ($order->getTotal() > 0)
						$report[$user->name][$order->date] = $order->getTotal();
				}

				// Remove if there aren't orders
				if (count($report[$user->name]) == 0)
					unset($report[$user->name]);
			}
		}

		$this->vars['company_name'] = $company_name;
		$this->vars['report'] = $report;
		$this->vars['date'] = CarbonDate::now()->format('d/m/Y à\s H:m');
		$this->vars['range'] = $range;
	}



	public function mountTags($ids, $date, $size) {
		$html = '';
		$count = 0;
		$companies = Company::find($ids);

		// Configura page layout
		if ($size == 'small') {
			$columns = 7;
			$rows = 18;
			$styles = 'border: .05mm solid white; padding: 2mm; height: 15mm;';
			$fontSize = '9px';
		} elseif ($size == 'medium') {
			$columns = 5;
			$rows = 18;
			$styles = 'border: .05mm solid white; padding: 2mm; height: 21mm;';
			$fontSize = '9px';
		} else {
			$columns = 3;
			$rows = 9;
			$styles = 'height: 31mm; border: .05mm solid white; padding: 10px;';
			$fontSize = '11px';
		}

		// Date
		$html .= '<h5 style="position: absolute; right: 8mm; top: 3mm; margin: 0;">' .
			\Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y')
			. '</h5>';

		$html .= '<table width="100%" style="border-collapse: collapse;">';

		foreach($companies as $company) {
			$data = Order::where('date', '=', $date)
				->whereHas('user', function ($query) use ($company) {
					return $query->where('company_id', $company->id);
				})
				->get();

			if (count($data) == 0)
				continue;

			// Filter data
			$data = $data->filter(function($order) {
				$products = $order->menuproducts->filter(function($product) {
					return $product->pivot->quantity > 0;
				});

				// Get only if products are greater than 0
				return count($products) > 0;
			})->values();

			// Rows loop
			for ($j = 0; $j < ceil((count($data) + 1) / $columns); $j++) {
				// Prevent last blank row
				if (!isset($data[$count]))
					break;

				$html .= '<tr>';

				// Columns loop
				for ($i = 0; $i < $columns; $i++) {
					$html .= '<td width="' . 100 / $columns . '%" style="vertical-align: ' . ($i == 0 && $j == 0 ? 'middle; text-align: center;' : 'top') . '; font-size: ' . $fontSize . '; ' . $styles . '">';

					if ($i == 0 && $j == 0) {
						$html .= '<b style="font-size: ' . ($size == 'small' ? 10 : 14) . 'px; margin-top: 10px;">' .
							$company->name .
							'</b>';
					} else {
						if (isset($data[$count])) {
							// Get products that has quantity greater than 0
							$products = $data[$count]->menuproducts->filter(function($product) {
								return $product->pivot->quantity > 0;
							});

							$html .= '<b>' . $data[$count]->user->name . '</b>';

							if (count($products) > 3 && $size == 'small') {
								$html .= '<div style="color: red">4 ou mais produtos!</div>';
							} else {
								// Product names logic
								$k = 0;
								$cols = $size == 'large' ? 2 : 1;
								$html .= '<table width="100%" style="border-collapse: collapse;">';

								// Include product's names
								foreach ($products as $menuProduct) {
									if ($k % $cols == 0)
										$html .= '<tr>';

									$html .= '<td width="' . 100 / $cols . '%" style="padding: 0; vertical-align: top">';
									$html .= '<span style="font-size: ' . $fontSize . ';">• ' . $menuProduct->product->name . ' x' . $menuProduct->pivot->quantity . '</span>';
									$html .= '</td>';

									if ($k >= $cols)
										$html .= '</tr>';

									$k++;
								}
								$html .= '</table>';
							}

							$count++;
						}
					}
					$html .= '</td>';
				}
				$html .= '</tr>';
			}

			$count = 0;
		}

		$html .= '</table>';

		return $html;
	}

	public function tags() {
		$html = '';
		$padding = 2;

		switch ($_GET['size'])
		{
		case 'small':
			$args = [
				'margin_top' => 14,
				'margin_right' => 8,
				'margin_bottom' => 14,
				'margin_left' => 8
			];
			break;
		case 'medium':
			$args = [
				'margin_top' => 12,
				'margin_right' => 4,
				'margin_bottom' => 16 - $padding * 2,
				'margin_left' => 6
			];
			break;
		case 'large':
			$args = [
				'margin_top' => 10,
				'margin_right' => 8,
				'margin_bottom' => 18 - $padding * 2,
				'margin_left' => 8
			];
			break;
		}

		$mpdf = new Mpdf($args);

		if (!isset($_GET['companies']))
			return 'Selecione pelo menos uma empresa.';

		$html = $this->mountTags(array_keys($_GET['companies']), substr($_GET['date'], 0, -9), $_GET['size']);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}
