<?php namespace DonaFruta\Corporative\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DonaFruta\Corporative\Widgets\ReportProducts;
use DonaFruta\Corporative\Widgets\ReportDate;
use DonaFruta\Corporative\Widgets\ReportCompany;
use DonaFruta\Corporative\Models\Company;

/**
 * Report Back-end Controller
 */
class Report extends Controller
{
	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('DonaFruta.Corporative', 'report');

		$reportCompany = new ReportCompany($this, []);
		$reportCompany->bindToController();

		$reportProducts = new ReportProducts($this, []);
		$reportProducts->bindToController();

		$reportDate = new ReportDate($this, []);
		$reportDate->bindToController();
	}

	public function preparation()
	{
		//
	}

	public function detail()
	{
		//
	}

	public function management()
	{
		//
	}

	public function financial()
	{
		//
	}
    
    public function missOrder()
	{
		//
	}

	public function tags()
	{
		$this->vars['companies'] = Company::all();
	}
}
