<?php namespace DonaFruta\Corporative\Tests\Models;

use DonaFruta\Corporative\Models\Order;
use \Carbon\Carbon;
use PluginTestCase;

class OrderTest extends PluginTestCase
{
	public function testGet()
	{
		$week = Carbon::now()->weekOfYear;
		$orders = Order::getByWeekNumber($week);

		// Assert a week has exactly seven orders
		$this->assertEquals($orders->count(), 7);
	}
    
	public function testTotal(){
		$order = Order::find(2);

		// Assert total order is 60
		// Assuming quantity is 3 and value is 5
		$this->assertEquals($order->getTotal(), 60);
	}
}
