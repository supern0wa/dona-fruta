<?php namespace DonaFruta\Corporative\Tests\Models;

use DonaFruta\Corporative\Models\User;
use \Carbon\Carbon;
use PluginTestCase;

class UserTest extends PluginTestCase
{
	public function setUp()
	{
		\Artisan::call('plugin:refresh', ['' => 'RainLab.User']);

		parent::setUp();
	}

    public function testGetDetails()
	{
        $user = new User();
		dd($user->getUserWeekDetails(3));
	}
    
	public function testGetStatementByMonth()
	{
		$month = Carbon::now()->month;
		$user = User::find(1);
		$statement = $user->getStatementByMonth($month);
        

		$this->assertEquals($statement['total_spent'], 420);
		$this->assertEquals($statement['remaining'], 180);
	}
}
