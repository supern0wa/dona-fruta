<?php namespace DonaFruta\Corporative\Tests\Models;

use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\ProductCategory;
use DonaFruta\Corporative\Models\Product;
use \Carbon\Carbon;
use PluginTestCase;

class MenuProductTest extends PluginTestCase
{
	public function testGetMenuProduct()
	{
		MenuProduct::getProducts();
        $this->assertArrayHasKey('Pasteis Natural', ['base_value']);
	}
    
    public function testGetMenu(){
        Menu::getMenu(1);
    }
}
