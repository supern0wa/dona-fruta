<?php namespace DonaFruta\Corporative\Models;

use Model;
use DonaFruta\Corporative\Models\Company;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\Product;

/**
 * Model
 */
class Company extends Model
{
	use \October\Rain\Database\Traits\Validation;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	protected $with = ['menu'];
	/**
	 * @var array Validation rules
	 */
	public $rules = [
		'name' => 'required|min:3',
		'status' => 'required',
		'cnpj' => 'required|unique:donafruta_corporative_company|cnpj',
		'included' => 'required',
		'credit' => 'required',
		'status' => 'required',
		'tipo_pagamento' => 'required'
	];

	public $attributeNames = [
		'name' => 'Nome',
		'credit' => 'Crédito',
		'cnpj' => 'CNPJ',
		'included' => 'Data de Inclusão',
		'status' => 'Status',
		'credit' => 'Valor do Crédito',
		'tipo_pagamento' => 'Tipo de Pagamento'
	];

	public $customMessages = [
		'cnpj' => 'O número de CNPJ não é válido.'
	];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'donafruta_corporative_company';

	public $belongsTo = [
		'address' => ['donafruta\corporative\models\Address'],
	];

	public $hasMany = [
		'user' => [
			'donafruta\corporative\models\User',
			'table' => 'users',
		],
	];

	public $attachMany = [
		'user' => ['donafruta\corporative\models\User', 'table' => 'users',],
        
	];

	public $hasOne = [
		'menu' => ['donafruta\corporative\models\Menu'],
	];

	public function afterUpdate() {
		$menuProducts = $_POST['menu'];

		foreach($menuProducts as $i => $v){
			MenuProduct::where('id', $i)->update(['diff_value' => $v]);
		}
	}

	public function afterCreate()
	{
		$products = Product::all();
		$menu = new Menu();
		$menu->company_id = $this->id;
		$menu ->save();

		foreach($products as $product){
			$menuProduct = new MenuProduct();
			$menuProduct->menu_id = $menu->id;
			$menuProduct->product_id = $product->id;
			$menuProduct->diff_value = $product->base_value;
			$menuProduct->save();
		}
	}

}
