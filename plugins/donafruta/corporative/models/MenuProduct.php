<?php namespace DonaFruta\Corporative\Models;

use Model;
use DonaFruta\Corporative\Models\ProductCategory;
use DonaFruta\Corporative\Models\Product;
use DonaFruta\Corporative\Helper;

/**
 * Model
 */
class MenuProduct extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    protected $with = ['product'];
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'donafruta_corporative_menu_products';
    
    public $belongsToMany = [
		'menu' => ['donafruta\corporative\models\Menu'],
	];
    
    public $belongsTo = [
        'product' => ['donafruta\corporative\models\Product'],
    ];

		public function getNameAttribute($value)
		{
			return $this->product->name;
		}

	public function getDiffValueAttribute($value)
	{
		return Helper::tofloat($value);
	}
    
    //DEPRECATED
    public static function getProducts(){
        $categories_id = ProductCategory::all();
        $arrayMenu = [];
        foreach($categories_id as $cat){
            $products = Product::where('category_id', $cat->id)->get();
            foreach($products as $prod){
                $menuProductItem = MenuProduct::where('product_id', $prod->id)->first();
                $arrayMenu[$cat->name][] = [
                    'id' => $menuProductItem->id,
                    'name' => $prod->name,
                    'price' => $prod->base_value,
                    'suspended' => $prod->suspended == 1 ? true : false,
                    'featured' => $prod->featured == 1 ? true : false,
                ];
            }
        }
//        dd($arrayMenu);
    }
    
    public static function changeStatus($arrayMenuProducts){
        foreach($arrayMenuProducts as $index => $status){
            $item = self::find($index);
            if($status == false){
                $item->show = 0;
                $item->save();
            }else{
                $item->show = 1;
                $item->save();
            }
        }
    }
}
