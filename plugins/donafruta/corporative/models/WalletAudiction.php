<?php namespace DonaFruta\Corporative\Models;

use Model;

/**
 * Model
 */
class WalletAudiction extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    protected $fillable = [
		'user_id',
		'responsable_id',
		'old_credit',
		'new_credit',
	];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'donafruta_corporative_wallets_audiction';
}
