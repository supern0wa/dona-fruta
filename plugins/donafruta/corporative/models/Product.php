<?php namespace DonaFruta\Corporative\Models;

use Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Dropbox\Client;
use DonaFruta\Corporative\Helper;

/**
 * Model
 */
class Product extends Model
{
	use \October\Rain\Database\Traits\Validation;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	protected $with = ['category'];

	/**
	 * @var array Validation rules
	 */
	public $rules = [
		'name' => 'required|min:3',
		'base_value' => 'required',
		'calories' => 'integer',
		'barcode' => 'required|integer|min:13|unique:donafruta_corporative_products'
	];

	public $attributeNames = [
		'name' => 'Nome',
		'base_value' => 'Valor Base',
		'calories' => 'Calorias',
		'barcode' => 'Código de Barra'
	];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'donafruta_corporative_products';

	public $belongsTo = [
		'category' => ['donafruta\corporative\models\ProductCategory'],
	];

	public function getCategoryIdOptions() {
		return ProductCategory::all()->lists('name', 'id');
	}

	public function getBaseValueAttribute($num) {
		return Helper::tofloat($num);
	}

    public function afterCreate()
	{
		$menus = Menu::all()->lists('id');
		foreach($menus as $m){
			$menuProduct = new MenuProduct();
			$menuProduct->menu_id = $m;
			$menuProduct->product_id = $this->id;
			$menuProduct->diff_value = $this->base_value;
			$menuProduct->save();
		}
	}
}
