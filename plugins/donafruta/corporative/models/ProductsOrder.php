<?php namespace DonaFruta\Corporative\Models;

use Model;

/**
 * Model
 */
class ProductsOrder extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'donafruta_corporative_products_order';
    
    
    public $hasOne = [
        'order' => ['donafruta\corporative\models\Order']
    ];
    
    
}
