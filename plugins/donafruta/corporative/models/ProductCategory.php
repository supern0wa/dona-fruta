<?php namespace DonaFruta\Corporative\Models;

use Model;

/**
 * Model
 */
class ProductCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
//    use \October\Rain\Database\Traits\Sortable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|unique:donafruta_corporative_product_category'
    ];
    
    public $attributeNames = [
		'name' => 'Nome'
	];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'donafruta_corporative_product_category';
    
    public $hasOne = [
        'product' => ['donafruta\corporative\models\Product'],
    ];
}
