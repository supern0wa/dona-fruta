<?php namespace DonaFruta\Corporative\Models;

use Model;
use DonaFruta\Corporative\Models\User;

/**
 * Model
 */
class Menu extends Model
{
	use \October\Rain\Database\Traits\Validation;

	/*
	 * Disable timestamps by default.
	 * Remove this line if timestamps are defined in the database table.
	 */
	public $timestamps = false;

	protected $with = ['menuproduct'];

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $hasOne = [
		'menuproduct' => ['donafruta\corporative\models\MenuProduct'],
	];

	public $hasMany = [
		'menuproducts' => ['donafruta\corporative\models\MenuProduct'],
	];

	public $belongsTo = [
		'company' => ['donafruta\corporative\models\Company'],
	];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'donafruta_corporative_menu';

	public static function getMenu($company_id){
		$menu = Menu::where('company_id', $company_id)->first();
		$menuProducts = MenuProduct::where('menu_id', $menu->id)->get();

		return $menuProducts;
	}
}
