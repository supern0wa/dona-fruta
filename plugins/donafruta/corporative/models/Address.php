<?php namespace DonaFruta\Corporative\Models;

use Model;

/**
 * Model
 */
class Address extends Model {
	use \October\Rain\Database\Traits\Validation;

	/*
	 * Disable timestamps by default.
	 * Remove this line if timestamps are defined in the database table.
	 */

	public $timestamps = false;

	/**
	 * @var array Validation rules
	 */
	public $rules = [
		'street' => 'required',
		'neighbourhood' => 'required',
		'state' => 'required',
		'city' => 'required',
		'number' => 'required',
		'zipcode' => 'required',
	];

	public $attributeNames = [
		'street' => 'Rua',
		'number' => 'Número',
		'state' => 'Estado',
		'city' => 'Cidade',
		'neighbourhood' => 'Bairro',
        'zipcode' => 'CEP'
        
	];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'donafruta_corporative_address';

	public $hasOne = [
		'company' => ['DonaFruta\Corporative\Models\Company'],
	];
}
