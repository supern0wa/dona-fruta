<?php namespace DonaFruta\Corporative\Models;

use Model;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon as CarbonDate;
use DonaFruta\Corporative\Helper;

/**
 * Model
 */
class Order extends Model
{
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var array Validation rules
	 */
	public $rules = [
		//
	];

	protected $with = ['menuproducts', 'user'];

	protected $fillable = ['user_id', 'date', 'status', 'paid'];

	protected $casts = [
		'paid' => 'boolean'
	];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'donafruta_corporative_orders';

	public $belongsTo = [
		'user' => ['donafruta\corporative\models\User']
	];

	public $belongsToMany = [
		'menuproducts' => [
			'donafruta\corporative\models\MenuProduct',
			'table' => 'donafruta_corporative_products_order',
			'otherKey' => 'menu_product_id',
			'pivot' => [
				'quantity',
				'current_value'
			]
		]
	];

	public static function getByWeekNumber($week, $user_id, $nonPaid = false){
		CarbonDate::setWeekStartsAt(CarbonDate::SUNDAY);
		$date = CarbonDate::now(); // or $date = new Carbon();
		$date->setISODate($date->year,$week); // 2016-10-17 23:59:59.000000
		$firstDay = $date->copy()->startOfWeek();
		$lastDay = $date->copy()->endOfWeek();

		$orders = Order::where('user_id', '=', $user_id)
			->where('date', '>=', $firstDay->toDateString())
			->where('date', '<', $lastDay->toDateString());
        
        if($nonPaid == true)
            $orders->where('paid', 0);

		return $orders;
	}

	public function getTotal(){
		$amoutPrice = 0;
		foreach ($this->menuproducts as $product) {
			if ($product->pivot->current_value != null) {
                $math = $product->pivot->quantity * Helper::tofloat($product->pivot->current_value);
				$amoutPrice += $math;
			} else {
                $math = $product->pivot->quantity * $product->diff_value;
				$amoutPrice += $math;
			}
		}

		return Helper::tofloat($amoutPrice);
	}

	public static function isLastMonthDay($date){
		CarbonDate::setWeekStartsAt(CarbonDate::SUNDAY);
		$date = CarbonDate::now();
		$firstWeekDay = $date->copy()->startOfWeek();
		$lastWeekDay = $date->copy()->endOfWeek();
		$lastMonthDay = CarbonDate::createFromDate($date->year, $date->month, $date->daysInMonth);

		while ($firstDay->toDateString() != $lastDay->toDateString()) {
			if ($firstDay->toDateString() == $lastMonthDay->toDateString()) {

			}
		}
	}
    
    public static function ordemPreparacaoReport($filters = array()){
        $query = DB::table('donafruta_corporative_products_order')
                 ->leftJoin('donafruta_corporative_orders', 'donafruta_corporative_orders.id', '=', 'donafruta_corporative_products_order.order_id')
                 ->leftJoin('users', 'users.id', '=', 'donafruta_corporative_orders.user_id')
                 ->leftJoin('donafruta_corporative_company', 'donafruta_corporative_company.id', '=', 'users.company_id')
                 ->leftJoin('donafruta_corporative_menu_products', 'donafruta_corporative_menu_products.id', '=', 'donafruta_corporative_products_order.menu_product_id')
                 ->leftJoin('donafruta_corporative_products', 'donafruta_corporative_products.id', '=', 'donafruta_corporative_menu_products.product_id')
                 ->select('donafruta_corporative_products.name as product_name','donafruta_corporative_company.name as company_name' ,'donafruta_corporative_products_order.current_value as price' ,DB::raw('sum(donafruta_corporative_products_order.quantity) as total'))
                 ->where('donafruta_corporative_products_order.quantity', '<>', 0);
                 
                if (array_key_exists('init', $filters) && array_key_exists('end', $filters)) {
                    $initDate = new CarbonDate($filters['init']);
                    $endDate = new CarbonDate($filters['end']);
                    $query->where('donafruta_corporative_orders.date', '>=', $initDate->toDateString())
                          ->where('donafruta_corporative_orders.date', '<=', $endDate->toDateString());
                } else {
                    $date = CarbonDate::now();
                    $query->where('donafruta_corporative_orders.date', '=', $date->toDateString());
                }
                
                if(array_key_exists('category', $filters)){
                    $query->whereIn('donafruta_corporative_products.id', $filters['products']);
                }
                
                if(array_key_exists('products', $filters)){
                    $query->whereIn('donafruta_corporative_products.id', array_values($filters['products']));
                }
                
                if(array_key_exists('company', $filters)){
                    $query->where('donafruta_corporative_company.id', $filters['company']);
                }
                    
                $query->groupBy('donafruta_corporative_products.id', 'donafruta_corporative_products.name');
                $query->orderBy('donafruta_corporative_products.name', 'ASC');
        
        return $query->get();
    }
    
    public static function detalhadoPedidosReport($filters = array()){
        $query = DB::table('donafruta_corporative_products_order')
                 ->leftJoin('donafruta_corporative_orders', 'donafruta_corporative_orders.id', '=', 'donafruta_corporative_products_order.order_id')
                 ->leftJoin('users', 'users.id', '=', 'donafruta_corporative_orders.user_id')
                 ->leftJoin('donafruta_corporative_company', 'donafruta_corporative_company.id', '=', 'users.company_id')
                 ->leftJoin('donafruta_corporative_menu_products', 'donafruta_corporative_menu_products.id', '=', 'donafruta_corporative_products_order.menu_product_id')
                 ->leftJoin('donafruta_corporative_products', 'donafruta_corporative_products.id', '=', 'donafruta_corporative_menu_products.product_id')
                 ->select('users.name as colaborador', 'donafruta_corporative_company.name as company_name','donafruta_corporative_products.name as product_name', DB::raw('sum(donafruta_corporative_products_order.quantity) as quantity'),'donafruta_corporative_products_order.current_value as price')
				 ->where('donafruta_corporative_products_order.quantity', '<>', 0)
				 ->orderBy('colaborador', 'ASC')
				 ->orderBy('donafruta_corporative_products.name', 'ASC');
                 
                if (array_key_exists('init', $filters) && array_key_exists('end', $filters)) {
                    $initDate = new CarbonDate($filters['init']);
                    $endDate = new CarbonDate($filters['end']);
                    $query->where('donafruta_corporative_orders.date', '>=', $initDate->toDateString())
                          ->where('donafruta_corporative_orders.date', '<=', $endDate->toDateString());
                } else {
                    $date = CarbonDate::now();
                    $query->where('donafruta_corporative_orders.date', '=', $date->toDateString());
                }
                
                if(array_key_exists('products', $filters)){
                    $query->whereIn('donafruta_corporative_products.id', array_values($filters['products']));
                }
                
                if(array_key_exists('company', $filters)){
                    $query->where('donafruta_corporative_company.id', $filters['company']);
                }
                if(array_key_exists('users', $filters)){
                    $ids = array_keys($filters['users']);
                    $query->whereIn('users.id', array_values($ids));
                }
                    
                $query->groupBy('users.id', 'donafruta_corporative_products_order.menu_product_id');
        
        return $query->get();
    }
    
    public static function gerencialPedidosReport($filters = array()){
        $query = DB::table('donafruta_corporative_products_order')
                 ->leftJoin('donafruta_corporative_orders', 'donafruta_corporative_orders.id', '=', 'donafruta_corporative_products_order.order_id')
                 ->leftJoin('donafruta_corporative_menu_products', 'donafruta_corporative_menu_products.id', '=', 'donafruta_corporative_products_order.menu_product_id')
                 ->leftJoin('donafruta_corporative_products', 'donafruta_corporative_products.id', '=', 'donafruta_corporative_menu_products.product_id')
                 ->leftJoin('donafruta_corporative_product_category', 'donafruta_corporative_product_category.id', '=', 'donafruta_corporative_products.category_id')
                 ->select('donafruta_corporative_product_category.name as category_name','donafruta_corporative_products.name as product_name', DB::raw('sum(donafruta_corporative_products_order.quantity) as quantity'),'donafruta_corporative_products_order.current_value as price')
                 ->where('donafruta_corporative_products_order.quantity', '<>', 0);
                 
                if (array_key_exists('init', $filters) && array_key_exists('end', $filters)) {
                    $initDate = new CarbonDate($filters['init']);
                    $endDate = new CarbonDate($filters['end']);
                    $query->where('donafruta_corporative_orders.date', '>=', $initDate->toDateString())
                          ->where('donafruta_corporative_orders.date', '<=', $endDate->toDateString());
                } else {
                    $date = CarbonDate::now();
                    $query->where('donafruta_corporative_orders.date', '=', $date->toDateString());
                }
                
                if(array_key_exists('products', $filters)){
                    $query->whereIn('donafruta_corporative_products.id', array_values($filters['products']));
                }
                    
                $query->groupBy('donafruta_corporative_products.id');
                $query->orderBy('donafruta_corporative_product_category.name', 'ASC');
                $query->orderBy('donafruta_corporative_products.name', 'ASC');
                
        
        return $query->get();
    }
    
    public static function financeiroReport($filters = array()){
        $query = DB::table('donafruta_corporative_products_order')
                 ->leftJoin('donafruta_corporative_orders', 'donafruta_corporative_orders.id', '=', 'donafruta_corporative_products_order.order_id')
                 ->leftJoin('users', 'users.id', '=', 'donafruta_corporative_orders.user_id')
                 ->leftJoin('donafruta_corporative_company', 'donafruta_corporative_company.id', '=', 'users.company_id')
                 ->leftJoin('donafruta_corporative_menu_products', 'donafruta_corporative_menu_products.id', '=', 'donafruta_corporative_products_order.menu_product_id')
                 ->leftJoin('donafruta_corporative_products', 'donafruta_corporative_products.id', '=', 'donafruta_corporative_menu_products.product_id')
                 ->select('users.name as colaborador', 'donafruta_corporative_company.name as company_name','donafruta_corporative_orders.date as date', DB::raw('sum(donafruta_corporative_products_order.quantity) as quantity'),'donafruta_corporative_products_order.current_value as price')
                 ->where('donafruta_corporative_products_order.quantity', '<>', 0);
                 
                if (array_key_exists('init', $filters) && array_key_exists('end', $filters)) {
                    $initDate = new CarbonDate($filters['init']);
                    $endDate = new CarbonDate($filters['end']);
                    $query->where('donafruta_corporative_orders.date', '>=', $initDate->toDateString())
                          ->where('donafruta_corporative_orders.date', '<=', $endDate->toDateString());
                } else {
                    $date = CarbonDate::now();
                    $query->where('donafruta_corporative_orders.date', '=', $date->toDateString());
                }
              
                if(array_key_exists('company', $filters)){
                    $query->where('donafruta_corporative_company.id', $filters['company']);
                }
                
                if(array_key_exists('users', $filters)){
                    $ids = array_keys($filters['users']);
                    $query->whereIn('users.id', array_values($ids));
                }
                    
                $query->groupBy('users.id', 'donafruta_corporative_products_order.menu_product_id');
                $query->orderBy('users.name', 'ASC');
                $query->orderBy('donafruta_corporative_orders.date');
                
        
        return $query->get();
    }
    
    
    public static function ausenciaPedidosReport($filters = array()){
			$return = [];

			if ($filters['company'] == 'all') {
				$users = User::where('status', 1)->get();
			} else {
				if (isset($filters['users'])) {
					$ids = array_keys($filters['users']);
					$users = User::whereIn('id', array_values($ids))->get();
				} else {
					$users = User::where('company_id', $filters['company'])
						->where('status', 1)
						->get();
				}
			}

			foreach ($users as $user) {
				$valid = true;
				$initDate = new CarbonDate($filters['init']);
				$endDate = new CarbonDate($filters['end']);
				$orders = Order::where('user_id', $user->id)
					->where('date', '>=', $initDate->toDateString())
					->where('date', '<=', $endDate->toDateString())
					->get();

				foreach ($orders as $order) {
					 if(count($order->menuproducts) > 0)
						 $valid = false;
				}

				if ($valid)
					if (isset($filters['display']) && $filters['display'] == 'all')
						$return[] = $user;
					else
						$return[$user->name] = $user->company->name;
			}
			
			return $return;
    }
    
    public static function nonPaidOrders($user_id){
			$orders = Order::where('paid', 0)
				->where('user_id', $user_id)->get();
			$nonPaidOrders = [];

            foreach($orders as $order):
                $sum = 0;

                if($order->getTotal() == 0)
                  continue;

                if(count($order->menuproducts) != 0):
                    foreach($order->menuproducts as $product):
                        $sum += $product->pivot->quantity * $product->pivot->current_value;
                    endforeach;
                endif;
                $dateFormat = CarbonDate::createFromFormat('Y-m-d', $order->date)->format('d/m/Y');
								$nonPaidOrders[] = [
									'id' => $order->id,
									'date' => $dateFormat,
									'total' => $sum,
									'updated_at' => CarbonDate::createFromFormat('Y-m-d H:i:s', $order->updated_at)->format('d/m/Y')
								];
            endforeach;
           
			return $nonPaidOrders;
    }
    
    public static function paidOrders($user_id){
			$orders = Order::where('paid', 1)
				->where('user_id', $user_id)->get();
			$paidOrders = [];

            foreach($orders as $order):
                $sum = 0;

                if($order->getTotal() == 0)
                  continue;

                if(count($order->menuproducts) != 0):
                    foreach($order->menuproducts as $product):
                        $sum += $product->pivot->quantity * $product->pivot->current_value;
                    endforeach;
                endif;
                $dateFormat = CarbonDate::createFromFormat('Y-m-d', $order->date)->format('d/m/Y');
								$paidOrders[] = [
									'id' => $order->id,
									'date' => $dateFormat,
									'total' => $sum,
									'updated_at' => CarbonDate::createFromFormat('Y-m-d H:i:s', $order->updated_at)->format('d/m/Y')
								];
            endforeach;
           
			return $paidOrders;
    }
    
    
    public static function tagsReport($filters = array()){
        $query = DB::table('donafruta_corporative_products_order')
                 ->leftJoin('donafruta_corporative_orders', 'donafruta_corporative_orders.id', '=', 'donafruta_corporative_products_order.order_id')
                 ->leftJoin('users', 'users.id', '=', 'donafruta_corporative_orders.user_id')
                 ->leftJoin('donafruta_corporative_company', 'donafruta_corporative_company.id', '=', 'users.company_id')
                 ->leftJoin('donafruta_corporative_menu_products', 'donafruta_corporative_menu_products.id', '=', 'donafruta_corporative_products_order.menu_product_id')
                 ->leftJoin('donafruta_corporative_products', 'donafruta_corporative_products.id', '=', 'donafruta_corporative_menu_products.product_id')
                 ->select('users.name as colaborador', 'donafruta_corporative_company.name as company_name','donafruta_corporative_products.name as product_name')
                 ->where('donafruta_corporative_products_order.quantity', '<>', 0);
                 
                if (array_key_exists('init', $filters) && array_key_exists('end', $filters)) {
                    $initDate = new CarbonDate($filters['init']);
                    $endDate = new CarbonDate($filters['end']);
                    $query->where('donafruta_corporative_orders.date', '>=', $initDate->toDateString())
                          ->where('donafruta_corporative_orders.date', '<=', $endDate->toDateString());
                } else {
                    $date = CarbonDate::now();
                    $query->where('donafruta_corporative_orders.date', '=', $date->toDateString());
                }
                
                if(array_key_exists('company', $filters)){
                    $query->where('donafruta_corporative_company.id', $filters['company']);
                }
                if(array_key_exists('users', $filters)){
                    $ids = array_keys($filters['users']);
                    $query->whereIn('users.id', array_values($ids));
                }
                
                dd($query->get());
        
        return $query->get();
    }
    
    
}
