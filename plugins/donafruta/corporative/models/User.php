<?php

namespace DonaFruta\Corporative\Models;

use Model;
use \Carbon\Carbon as CarbonDate;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\Product;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\WalletAudiction;
use DonaFruta\Corporative\Helper;
use JWTAuth;

/**
 * Model
 */
class User extends \RainLab\User\Models\User {

	public $with = ['company'];

	public $belongsTo = [
		'company' => ['donafruta\corporative\models\Company']
	];

	/**
	 * Validation rules
	 */
	public $rules = [
		'name'     => 'required',
		'email'    => 'required|between:6,255|email|unique:users',
		'password' => 'required:create|between:4,255|confirmed',
		'password_confirmation' => 'required_with:password|between:4,255',
		'credit'   => 'required'
	];

	public $attributeNames = [
		'name' => 'Nome',
		'base_value' => 'Email',
		'calories' => 'Calorias',
		'barcode' => 'Código de Barra'
	];

	protected $fillable = [
		'name',
		'surname',
		'login',
		'username',
		'email',
		'password',
		'credit',
		'password_confirmation',
		'permission',
		'status',
		'company_id'
	];
    
    public $hasMany = [
        'orders' => ['donafruta\corporative\models\Order']
    ];

	public function getCreditAttribute($value) {
		return Helper::tofloat($value);
	}

	public function getDailyLimitAttribute($value) {
		return Helper::tofloat($value);
	}

	public function getCompanyIdOptions() {
		return Company::all()->lists('name', 'id');
	}

	public function getStatementByMonth($month = null, $id = null) {
		if ($month) {
			$date = new CarbonDate();
			$date->month = $month;
		} else {
			$date = CarbonDate::now();
		}

		if($id != null){
			$user_id = $id;
		}else{
			$user_id = $this->id;
		}

		$beginDate = CarbonDate::createFromDate($date->year, $date->month, 1);
		$endDate = CarbonDate::createFromDate($date->year, $date->month, $date->daysInMonth);
        
		$amountPrice = 0;
		$orders = Order::where('user_id', $user_id)
			->where('date', '>=', $beginDate->toDateString())
			->where('date', '<=', $endDate->toDateString())->get();
        
		foreach ($orders as $order) {
			$amountPrice += $order->getTotal();
		}
        
		$remaining = $this->credit - $amountPrice;

		return [
			'remaining' => Helper::tofloat($remaining),
			'total_spent' => Helper::tofloat($amountPrice)
		];
	}

	public function canBuy($data) {
		$valid = [];

		// Extract dates from orders
		$dates = array_keys($data);

		// Transform dates into Carbon object
		$dates = array_map(function ($value) {
			return CarbonDate::createFromFormat('Y-m-d', $value);
		}, $dates);

		// Verify if there's month break
		foreach ($dates as $date) {
			$formated = $date->format('Y-m-d');
			$split[$date->month][$formated] = $data[$formated];
		}

		// Loop through month orders
		foreach ($split as $month => $orders) {
			$total = 0;
			$statement = $this->getStatementByMonth($month);

			// Loop through order days
			foreach ($orders as $date => $items) {
				$products = MenuProduct::whereIn('id', array_keys($items))->get();

				foreach ($products as $product) {
					$quantity = $items[$product->id];
					$total += $quantity * $product->diff_value;
				}
			}

			// Verify if user can buy the products
			$valid[] = $statement['remaining'] >= $total;
		}

		// Return false if there are any failed
		return !in_array(false, $valid);
	}

	public static function authenticated() {
		try {
			if (!$user = JWTAuth::parseToken()->authenticate()) {
				return response(['error' => 'user_not_found'], 404);
			}
		} catch (JWTAuth\Exceptions\TokenExpiredException $e) {
			return response(['error' => 'token_expired'], $e->getStatusCode());
		} catch (JWTAuth\Exceptions\TokenInvalidException $e) {
			return response(['error' => 'token_invalid'], $e->getStatusCode());
		} catch (JWTAuth\Exceptions\JWTException $e) {
			return response(['error' => 'token_absent'], $e->getStatusCode());
		}

		return self::find($user->id);
	}

	public function getUserWeekDetails($month = null) {
		if ($month) {
			$carbon = new CarbonDate();
			$carbon->month = $month;
		} else {
			$carbon = CarbonDate::now();
		}

		$currentMonth = $carbon->month;

		$statement = $this->getStatementByMonth($currentMonth, $this->id);

        $beginDate = CarbonDate::createFromDate($carbon->year, $carbon->month, 1);
		$endDate = CarbonDate::createFromDate($carbon->year, $carbon->month, $carbon->daysInMonth);
		$orders = Order::where('user_id', $this->id)
			->where('date', '>=', $beginDate->toDateString())
			->where('date', '<=', $endDate->toDateString())->get();

		$ordersPerWeek = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
		foreach ($orders as $order) {
            $carbonOrderDate = new CarbonDate($order->date);
			if($carbonOrderDate->weekNumberInMonth != 0){
                $ordersPerWeek[$carbonOrderDate->weekNumberInMonth] += $order->getTotal();
            }
		}

		return [
			'available' => $statement['remaining'],
			'spent' => $statement['total_spent'],
			'credit' => $this->credit,
			'weeks' => $ordersPerWeek,
			'month' => $currentMonth
		];
	}
}
