<?php namespace DonaFruta\Corporative\Models;

use Model;

/**
 * Model
 */
class Revision extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'donafruta_corporative_revisions';
    
    protected $fillable = [
		'initial_date',
		'final_date',
		'responsable_id',
		'funcionario_id',
		'status'
	];

    public function getOrdersAttribute() {
        $orders = Order::where('user_id', $this->user_id)
            ->whereBetween('date', $this->initial_date, $this->final_date)
            ->get();

        return $orders;
    }

    public function getTotal() {
        $total = 0;

        foreach ($this->orders as $order) {
            $total += $order->getTotal();
        }

        return $total;
    }
}
