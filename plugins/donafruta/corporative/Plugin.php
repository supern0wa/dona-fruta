<?php namespace DonaFruta\Corporative;

use System\Classes\PluginBase;
use Event;
use Validator;
use DonaFruta\Corporative\Models\User;

class Plugin extends PluginBase
{
    public $require = [
		'RainLab.User'
    ];
    
	public function registerComponents()
	{
		//
	}

	public function registerSettings()
	{
		//
	}

	public function boot()
	{
		// TODO: Company list user filter
		$status = isset($_GET['status']) ? $_GET['status'] : 1;

		User::extend(function($model) use ($status) {
			$model->addDynamicMethod('scopeUserStatus', function ($query, $user) use ($status) {
				if ($status == 0 || $status == 1)
					$query = $query->where('status', '=', $status);

				return $query;
			});
		});

		Validator::extend('cnpj', function($attribute, $value, $parameters) {
			$cnpj = preg_replace('/[^0-9]/', '', (string) $value);
			// Valida tamanho
			if (strlen($cnpj) != 14)
				return false;
			// Valida primeiro dígito verificador
			for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
				return false;
			// Valida segundo dígito verificador
			for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
			return $value == 'foo';
		});

		Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
			// Add global scripts dependencies
			$controller->addJs(url('plugins/donafruta/corporative/assets/js/jquery.mask.min.js'));
			$controller->addJs(url('plugins/donafruta/corporative/assets/js/cleave.min.js'));

			// Add backend custom scripts & styles
			$controller->addJs(url('plugins/donafruta/corporative/assets/js/main.js'));
			$controller->addCss(url('plugins/donafruta/corporative/assets/css/main.css'));
		});
	}

	public function registerListColumnTypes()
	{
		return [
			'currency' => function($value) { 
				return 'R$ ' . number_format($value, 2, ',', '.');
			}
		];
	}
}
