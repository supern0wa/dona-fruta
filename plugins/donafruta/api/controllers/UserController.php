<?php namespace DonaFruta\Api\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\WalletAudiction;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\Revision;
use DonaFruta\Corporative\Models\MenuProduct;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends BaseController
{
	/**
	 * Retrieve the current authenticated user.
	 *
	 * @return void
	 */
	public function auth(Request $request)
	{
		$user = User::authenticated();

		return response([
			'user' => $user
		], 200);
	}

	public function wallet(Request $request, $month = null)
	{
		$user = User::authenticated();
		$wallet = $user->getUserWeekDetails($month);

		return response([
			'wallet' => $wallet
		], 200);
	}

	public function oddWallet(Request $request, $id, $month = null)
	{
		$user = User::find($id);
		$wallet = $user->getUserWeekDetails($month, $id);

		return response([
			'wallet' => $wallet
		], 200);
	}

	public function index(Request $request)
	{
		$user = User::authenticated();
		if($user){
			$users = User::where('company_id', $user->company_id)->get();
		}else{
			$users = [];
		}

		return response([
			'users' => $users
		], 200);
	}
    
    public function testWallet(){
        $user = User::authenticated();
        $wallet = $user->getStatementByMonth(4);
        dd($wallet);
    }
    
    public function userRevisions($id)
	{
		$revisions = Revision::where('funcionario_id', $id)->get(); 
		return response([
			'revisions' => $revisions
		], 200);
	}

	public function create(Request $request, $id = null){
		$userLogged = User::authenticated();
		if($userLogged->permission == 'Responsavel' ){
			$input = $request->all();
			$input['company_id'] = $userLogged->company_id;
			$oldCredit = 0;
			try{
				if(is_null($id)){
                    $input['status'] = 1;
					$user = User::create($input);
				}else{
					$user = User::find($id);
					$user->name = $input['name'];
					$user->email = $input['email'];
					$user->password = $input['password'];
					$user->password_confirmation = $input['password_confirmation'];
					if($input['credit'] != $user->credit){
						$oldCredit = $user->credit;
					}
					$user->credit = $input['credit'];
					$user->permission = $input['permission'];
					$user->save();

					if($oldCredit != 0){
						$walletAudiction = new WalletAudiction();
						$walletAudiction->user_id = $user->id;
						$walletAudiction->responsable_id = $userLogged->id;
						$walletAudiction->old_credit = $oldCredit;
						$walletAudiction->new_credit = $input['credit'];
						$walletAudiction->save();
					}
				}
			} catch (\Exception $ex) {
				return response(['error' => $ex->getMessage()], 400);
			}

			return response(['user' => $user], 200);
		}else{
			return response(['error' => 'Você não tem permissão para esta sessão'], 400);
		}
	}

	public function block(Request $request, $id){
		try{
			$user = User::find($id);
			if($user->status == 1){
				$user->status = 0;
			}else{
				$user->status = 1;
			}
			$user->save();
		} catch (Exception $ex) {
			return response(['error' => $ex->getMessage()], 400);
		}

		return response(['user' => $user], 200);
	}
    
    public function getTodayOrderItems(Request $request)
	{
        $today = \Carbon\Carbon::now();
		$userLogged = User::authenticated();
        $dayOrder = Order::where('user_id', '=', $userLogged->id)->where('date', '=', $today->toDateString())->first();
        if($dayOrder){
            $items = $dayOrder->menuproducts->filter(function ($item) {
                            if($item->pivot->quantity > 0){
                               return $item;
                            }
                     });
            if(count($items) > 0){         
                $data = [];        
                foreach($items as $item):
                    $data[$item->product->name] = $item->pivot->quantity;
                endforeach;
                return response(['items' => $data], 200);
            }else{
                return response(['items' => []], 200);
            }
        }else{
            return response(['items' => []], 200);
        }
	}

	public function orders(Request $request, $id, $week) {
		$orders = Order::getByWeekNumber($week, $id)->get();

		return response(['orders' => $orders], 200);
	}
    
    public function revision(Request $request){
        $userLogged = User::authenticated();
        $input = $request->all();
       
        try{
            $revision = new Revision();
            $revision->responsable_id = $userLogged->id;
            $revision->funcionario_id = $input['user'];
            $revision->initial_date = $input['initial'];
            $revision->final_date = $input['final'];
            $revision->status = 0;
            $revision->save();
        } catch (Exception $ex) {
			return response(['error' => $ex->getMessage()], 400);
		}
        
        return response(['revision' => $revision], 200);
        
    }
    
    public function updateRevision(Request $request){
        
    }
    
	public function resetPassword(Request $request){
		$userLogged = User::authenticated();
		$input = $request->all();

		if (\Hash::check($input['current'], $userLogged->password)) {
			if($input['password'] == $input['confirmation']){
				$userLogged->password = $input['password'];
				$userLogged->password_confirmation = $input['confirmation'];
				$userLogged->save();
			}else{
				return response(['message' => 'Os valores de senha devem ser iguais'], 400);
			}
		}else{
			return response(['message' => 'Digite sua senha atual corretamente'], 400);
		}

		return response(['user' => $userLogged], 200);
	}

   /**
	 * Retrieves non-paid orders from the active user.
	 *
	 * @return response - array Corporative\Models\Order
	 */
		public function pending(Request $request) {
			$pending = [];
			$user = User::authenticated();
			$orders = Order::where('user_id', $user->id)
				->where('date', '<=', date('Y-m-d'))
				->where('paid', 0)
				->get();

			foreach ($orders as $order) {
				$total = $order->getTotal();

				if ($total == 0)
					continue;

				$pending[] = [
					'id' => $order->id,
					'date' => $order->date,
					'total' => $total,
					'products' => $order->menuproducts->map(function($value) {
						return [
							'id' => $value->id,
							'name' => $value->product->name,
							'quantity' => $value->pivot->quantity,
							'value' => $value->pivot->current_value
						];
					})
				];
			}

			return response(['orders' => $pending], 200);
		}
}
