<?php

namespace DonaFruta\Api\Controllers;

use DonaFruta\Corporative\Models\Menu;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class MenuController extends BaseController
{
	public function getMenu(Request $request)
	{
		$user = User::authenticated();
		return response(Menu::getMenu($user->company->id), 200);
	}

	public function formExtendModel($model)
	{
		if (!$model->address)
			$model->address = new Address;

		return $model;
	}

	public function changeStatus(Request $request){
		$input = $request->all();
		MenuProduct::changeStatus($input['menu_products']);

		return response(200);
	}
}
