<?php namespace DonaFruta\Api\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\MenuProduct;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\Revision;

class OrderController extends BaseController
{
	/**
	 * Retrieves all orders from the active user.
	 *
	 * @return void
	 */
	public function index(Request $request)
	{
		return response(['test' => true], 200);
	}

	/**
	 * Retrieves orders from a specific week of the current year from the active user.
	 *
	 * @param $number|int - The week number
	 * @return response - array Corporative\Models\Order
	 */
	public function show(Request $request, $week)
	{
		$user = User::authenticated();
		$order = Order::getByWeekNumber($week, $user->id)->get();

		return response(['orders' => $order], 200);
	}
    
	/**
	 * Stores orders to the entire week.
	 *
	 * @return void
	 */
	public function store(Request $request)
	{
        $batch = [];
        $input = $request->all();
        $user = User::authenticated();

		if (!$input['orders'] || count($input['orders']) == 0)
			return response(['error' => 'Campo order obrigatório', 'success' => false], 400);
        
		if ($user->canBuy($input['orders']) == false)
			return response(['error' => 'Limite excedido', 'success' => false], 400);

		foreach ($input['orders'] as $date => $items) {
			$order = Order::create([
				'user_id' => $user->id,
				'date' => $date,
				'status' => 1
			]);

			foreach ($items as $id => $quantity) {
				if ($quantity == 0)
					continue;

				$menuProduct = MenuProduct::find($id);
                
				if ($menuProduct)
					$order->menuproducts()
						->add($menuProduct, [
							'quantity' => $quantity,
							'current_value' => $menuProduct->diff_value
						]);
			}

			$batch[] = $order->id;
		}

		// Get orders with relations
		$orders = Order::findMany($batch);

		return response(['success' => true, 'orders' => $orders], 200);
	}
    
    public function update(Request $request){
        $batch = [];
		$input = $request->all();
		$user = User::authenticated();
        
		if (!$input['orders'] || count($input['orders']) == 0)
			return response(['error' => 'Campo order obrigatório', 'success' => false], 400);

        // Find orders and delete them
		foreach ($input['orders'] as $date => $items) {
			$old = Order::where('user_id', $user->id)
                ->where('date', $date)->first();
            
            if ($old)
                $old->delete();
		}
        
        // FIXME
        // If the orders are deleted, and the user for some reason cannot buy it,
        // The previous orders data will be lost
        
        // Verify if user can actually buy it
        if ($user->canBuy($input['orders']) == false)
			return response(['error' => 'Limite excedido', 'success' => false], 400);
        
        foreach ($input['orders'] as $date => $items) {
			$order = Order::create([
				'user_id' => $user->id,
				'date' => $date,
				'status' => 1
			]);

			foreach ($items as $id => $quantity) {
				if ($quantity == 0)
					continue;

				$menuProduct = MenuProduct::find($id);

				if ($menuProduct)
					$order->menuproducts()
						->add($menuProduct, [
							'quantity' => $quantity,
							'current_value' => $menuProduct->diff_value
						]);
			}

			$batch[] = $order->id;
		}
        
        $revision = Revision::find($input['revision']);
        $revision->update(['status' => 1]);

		// Get orders with relations
		$orders = Order::findMany($batch);

		return response(['success' => true, 'orders' => $orders], 200);
    }
    
    public function testReport(){
        
        $filters = [
            'users' => [19,17],
//            'init' => '2018-05-01',
//            'end' => '2018-05-08'
        ];
        
        dd(Order::ausenciaPedidosReport($filters));
    }
}
