<?php

namespace DonaFruta\Api\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Spatie\Dropbox\Client;

class MediaController extends BaseController
{
	public function show(Request $request, $filename)
	{
		$client = new Client(env('DROPBOX_TOKEN'));
		$stream = $client->download('/' . $filename);
		$file = stream_get_contents($stream);

		return response($file, 200)
			->header('Content-Type', 'image/jpeg');
	}

	public function showFolder(Request $request, $folder, $filename)
	{
		$client = new Client(env('DROPBOX_TOKEN'));
		$stream = $client->download('/' . $folder . '/' . $filename);
		$file = stream_get_contents($stream);

		return response($file, 200)
			->header('Content-Type', 'image/jpeg');
	}
}
