<?php namespace DonaFruta\Api\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DonaFruta\Corporative\Models\User;
use DonaFruta\Corporative\Models\WalletAudiction;
use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\Revision;

class RevisionController extends BaseController
{
	public function index(Request $request){
		$user = User::authenticated();
		$revisions = Revision::where('funcionario_id', $user->id)
			->where('final_date', '>=', date('Y-m-d'))->get();

		return response(['revisions' => $revisions], 200);
	}

	public function store(Request $request){
		$userLogged = User::authenticated();
		$input = $request->all();

		try{
			$existsOrder = Order::whereBetween('date', [$input['initial'], $input['final']])
				->where('user_id', $input['user'])
				->first();

			if($existsOrder == null){
				return response(['error' => 'Não existem pedidos para esta data.'], 400);
			}
			// Search for existing revisions
			$exists = Revision::where('funcionario_id', $input['user'])
				->where('initial_date', $input['initial'])
				->where('final_date', $input['final'])
				->first();

			// Verify if revision already exists
			if ($exists) {
				$revision = $exists;

				if ($exists->status == 0){
					// Return error if status is 0
					$exists->update(['status' => 1]);
					$message = 'Revisão fechada com sucesso.';
				} elseif ($exists->status == 1) {
					// Updated its status to 0 otherwise
					$exists->update(['status' => 0]);
					//Close all other revisions from this user
					$close = Revision::where('id', '!=', $exists->id)->where('funcionario_id', $input['user'])->update(['status' => 1]);
					$message = 'Revisão aberta com sucesso.';
				}
			} else {
				// Create new revision
				$revision = Revision::create([
					'responsable_id' => $userLogged->id,
					'funcionario_id' => $input['user'],
					'initial_date' => $input['initial'],
					'final_date' => $input['final'],
					'status' => 0
				]);
				//Close all other revisions from this user
				$close = Revision::where('id', '!=', $revision->id)->where('funcionario_id', $input['user'])->update(['status' => 1]);
				$message = 'Revisão aberta com sucesso.';
			}
		} catch (Exception $ex) {
			return response(['error' => $ex->getMessage()], 400);
		}

		$revisions = Revision::where('funcionario_id', $input['user'])->get();

		return response([
			'revisions' => $revisions,
			'message' => $message
		], 200);
	}

	public function update(Request $request, $id){
		$revision = Revision::find($id);
		$revision->update(['status' => 1]);

		return response(['revision' => $revision], 200);
	}
}
