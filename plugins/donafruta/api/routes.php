<?php

Route::group(['prefix' => 'api'], function () {
	// Version 1
	Route::group(['prefix' => 'v1'], function () {
		Route::get('/date', function () {
			return response(['date' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')], 200);
		});

		Route::get('/fix-products', function () {
			$products = \DonaFruta\Corporative\Models\Product::all();

			foreach ($products as $product) {
				if (!$product->category) {
					$product->delete();
				}
			}

			$menu = \DonaFruta\Corporative\Models\MenuProduct::all();

			foreach ($menu as $item) {
				if (!$item->product) {
					$orders = \DonaFruta\Corporative\Models\ProductsOrder::where('menu_product_id', $item->id)->get();

					foreach ($orders as $order) {
						$order->delete();
					}

					$item->delete();
				}
			}
		});

		// Get media files route
		Route::group(['prefix' => 'uploads'], function () {
			Route::get('/{filename}', ['uses' => 'DonaFruta\Api\Controllers\MediaController@show']);
			Route::get('/{folder}/{filename}', ['uses' => 'DonaFruta\Api\Controllers\MediaController@showFolder']);
		});

		// JWT protected routes
		Route::group(['middleware' => '\Tymon\JWTAuth\Middleware\GetUserFromToken'], function () {
			// Authenticated User
			Route::group(['prefix' => 'user'], function () {
				Route::get('/', ['uses' => 'DonaFruta\Api\Controllers\UserController@auth']);
				Route::get('/wallet/{month?}', ['uses' => 'DonaFruta\Api\Controllers\UserController@wallet']);
				Route::post('/create', ['uses' => 'DonaFruta\Api\Controllers\UserController@create']);
				Route::patch('/update/{id}', ['uses' => 'DonaFruta\Api\Controllers\UserController@create']);
				Route::post('/block/{id}', ['uses' => 'DonaFruta\Api\Controllers\UserController@block']);
				// Route::get('/test', ['uses' => 'DonaFruta\Api\Controllers\UserController@testWallet']);
				Route::post('/reset', ['uses' => 'DonaFruta\Api\Controllers\UserController@resetPassword']);
				Route::get('/day-items', ['uses' => 'DonaFruta\Api\Controllers\UserController@getTodayOrderItems']);
				Route::get('/pending', ['uses' => 'DonaFruta\Api\Controllers\UserController@pending']);
			});

			// Users
			Route::group(['prefix' => 'users'], function () {
				Route::get('/', ['uses' => 'DonaFruta\Api\Controllers\UserController@index']);
				Route::get('/{id}/wallet/{month?}', ['uses' => 'DonaFruta\Api\Controllers\UserController@oddWallet']);
				Route::get('/{id}/revisions', ['uses' => 'DonaFruta\Api\Controllers\UserController@userRevisions']);
				Route::post('/create', ['uses' => 'DonaFruta\Api\Controllers\UserController@create']);
				Route::patch('/update/{id}', ['uses' => 'DonaFruta\Api\Controllers\UserController@create']);
				Route::post('/block/{id}', ['uses' => 'DonaFruta\Api\Controllers\UserController@block']);
				Route::get('/orders/{id}/{week}', ['uses' => 'DonaFruta\Api\Controllers\UserController@orders']);
			});

			// Revisions
			Route::group(['prefix' => 'revisions'], function () {
				Route::post('/', ['uses' => 'DonaFruta\Api\Controllers\RevisionController@store']);
				Route::get('/', ['uses' => 'DonaFruta\Api\Controllers\RevisionController@index']);
				Route::patch('/{id}', ['uses' => 'DonaFruta\Api\Controllers\RevisionController@update']);
			});

			// Orders
			Route::group(['prefix' => 'orders'], function () {
				Route::get('/', ['uses' => 'DonaFruta\Api\Controllers\OrderController@index']);
				Route::get('/week/{week}', ['uses' => 'DonaFruta\Api\Controllers\OrderController@show']);
				Route::post('/', ['uses' => 'DonaFruta\Api\Controllers\OrderController@store']);
				Route::patch('/', ['uses' => 'DonaFruta\Api\Controllers\OrderController@update']);
				Route::get('/report', ['uses' => 'DonaFruta\Api\Controllers\OrderController@testReport']);
			});

			// Menu
			Route::group(['prefix' => 'menu'], function () {
				Route::get('/', ['uses' => 'DonaFruta\Api\Controllers\MenuController@getMenu']);
				Route::post('/change-status', ['uses' => 'DonaFruta\Api\Controllers\MenuController@changeStatus']);
			});
		});
	});
});
