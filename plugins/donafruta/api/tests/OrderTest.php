<?php namespace DonaFruta\Api\Tests;

use DonaFruta\Corporative\Models\Order;
use DonaFruta\Corporative\Models\Menu;
use \Carbon\Carbon;
use PluginTestCase;
use Illuminate\Http\Response;

class OrderTest extends PluginTestCase
{
	protected $baseUrl = '/api/v1/orders';

	public function setup(){
		parent::setup();

		include plugins_path('donafruta/api/routes.php');
	}

	public function testStoreWeekSuccess()
	{
		/* [
			'2018-02-05' => [
				1 => 2,
				2 => 2
			]
		] */

		$orders = [
			[
				'date' => '2018-02-15',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 2
					],
					[
						'menu_product_id' => 2,
						'quantity' => 2
					],
				]
			],
			[
				'date' => '2018-02-16',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 2
					],
					[
						'menu_product_id' => 2,
						'quantity' => 2
					],
				]
			],
			[
				'date' => '2018-02-17',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 2
					],
					[
						'menu_product_id' => 2,
						'quantity' => 2
					],
				]
			],
		];

		$response = $this->json('POST', '/api/v1/orders/', [
			'orders' => $orders
		]);

		$content = json_decode($response->getContent(), true);

		$this->assertEquals(true, $content['success']);
		$this->assertArrayHasKey('orders', $content);
		$this->assertEquals('200', $response->getStatusCode());

	}
	public function testStoreWeekFails()
	{
		$orders = [
			[
				'date' => '2018-02-15',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 30
					],
					[
						'menu_product_id' => 2,
						'quantity' => 15
					],
				]
			],
			[
				'date' => '2018-02-16',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 2
					],
					[
						'menu_product_id' => 2,
						'quantity' => 15
					],
				]
			],
			[
				'date' => '2018-02-17',
				'items' => [
					[
						'menu_product_id' => 1,
						'quantity' => 120
					],
					[
						'menu_product_id' => 2,
						'quantity' => 532
					],
				]
			],
		];
		$response = $this->json('POST', '/api/v1/orders/', [
			'orders' => $orders
		]);

		$content = json_decode($response->getContent(), true);

		$this->assertEquals(false, $content['success']);
		$this->assertEquals('400', $response->getStatusCode());
	}
}
