<?php namespace DonaFruta\Api;

class Plugin extends \System\Classes\PluginBase
{
    
    public $require = [
        'DonaFruta.Corporative'
    ];
    
    public function pluginDetails()
    {
        return [
            'name' => 'API Plugin',
            'description' => 'Dona Fruta API endpoint.',
            'author' => 'Supernowa',
            'icon' => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            //
        ];
    }
}
